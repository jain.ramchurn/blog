---
title: "Projects"
draft: False
hidden: True
---

## [qdir](https://github.com/k3ii/qdir)  
**Quick Directory Generator**

---

## [git-cz](https://github.com/k3ii/git-cz)  
**A simple commitizen written in Rust**

---

## [revq](https://github.com/k3ii/revq)  
**CLI to search and filter GitHub pull requests**

---

## [kouran](https://github.com/k3ii/kouran)  
**CLI tool to view power outages in Mauritius**

---

## [conze](https://github.com/k3ii/conze)  
**CLI for viewing and comparing holidays across countries**

---

## [cloud-resume-challenge](https://github.com/k3ii/mscc-cloud-resume-challenge)  
**The Cloud Resume Challenge on AWS**

---

## [Linux mirrors in Mauritius](https://mirror.cloud.mu/)  
**Linux mirrors hosted in Mauritius**

