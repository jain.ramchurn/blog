---
title: "Sapiens"
date: 2022-07-26T15:16:30+04:00
tags: [ 'books', 'quotes']
categories: [ 'books']
draft: False
---

Sapiens: A Brief History of Humankind beautifully written by Yuval Noah Harari is an bold,engaging, and provocating book.
Below are excerpts and paragraphs which filled me with a sense of wonder and changed the way I think.


## Sapiens and Tolerance

Tolerance is not a Sapiens trademark.
In modern times, a small difference in skin colour, dialect or religion has been enough to prompt one group of Sapiens to set about exterminating another group.
Would ancient Sapiens have been more tolerant towards an entirely different human species? It may well be that when Sapiens encountered Neanderthals, the result was the first and most significant ethnic-cleansing campaign in history.

## Different human species coexist

Imagine how  things might have turned out had the Neanderthals or Denisovans survived alongside Homo sapiens.
What kind of cultures, societies and political structures would have emerged in a world where several different human species coexisted?
How, for example, would religious faiths have unfolded? Would the book of Genesis have declared that Neanderthals descend from Adam and Eve, would Jesus have died for the Sapiens
of Denisovans, and would the Qur'an have reserved seats in heaven for all righteous humans, whatever their species?
Would Neanderthals have been able to  serve in the Roman legions, or in the sprawling bureaucracy of imperial China? Would the American Declaration of Independence hold as a
self-evident truth that all members of the genus Homo are created equal? Would Karl Max have urged workers of all species to unite?

Over the past 10,000 years, Homo sapiens have grown so accustomed to being the only human species that it's hard for us to conceive of any other possibility.
Our lack of brothers and sisters makes it easier to imagine that we are the epitomes of creation, and that a chasm seperates us from the rest of animal kingdom. When Charles Darwin indicated that Homo sapiens were just another kind of animal,
people were  outraged. Even today, any refuse to believe it. Had the Neanderthals survived, would we still imagine ourselves to be a creature apart?
Perhaps this is exactly why our ancestors wiped out the Neanderthals. They were too familiar to ignore, but too different to tolerate.
