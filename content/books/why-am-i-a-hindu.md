---
title: "Why Am I a Hindu"
date: 2022-11-25T21:58:19+04:00
categories: [ 'books' ]
tags: [ 'books' ]
draft: false
---

*Why am I a Hindu* is written by Shashi Tharoor in which he write about the history of Hinduism and dispel many of the misconceptions of the Hinduism faith.


---

At the same time, as a Hindu, I appreciate the fact that Hinduism professes no false certitudes.
Its capacity to express wonder at Creation and simultaneously scepticism about the omniscience of the Creator are unique to Hinduism.
Both are captured beautifully in this verse from the 3,500-year-old Rig Veda, the Nasadiya Sukta or Creation Hymn:

*Then there was neither non-existence nor existence,*
*Then there was neither space, nor the sky beyond.*
*What covered it? Where was it?*
*What sheltered it? Was there water, in depths unfathomed?*

*Then there was neither death nor immortality*
*Nor was there then the division between night and day.*
*That One breathed, breathlessly and self-sustaining.*
*There was that One then, and there was no other.*

*In the beginning there was only darkness, veiled in darkness,*
*In profound darkness, a water without light.*
*All that existed then was void and formless.*
*That One arose at last, born of the power of heat.*

*In the beginning arose desire,*
*That primal seed, born of the mind.*
*The sages who searched their hearts with wisdom,*
*Discovered the link of the existent to the non-existent.*

*And they stretched their cord of vision across the void,*
*What was above? What was below?*
*Then seeds were sown and mighty power arose,*
*Below was strength, Above was impulse.*

*Who really knows? And who can say?*
*Whence did it all come? And how did creation happen?*
*The gods themselves are later than creation,*
*So who knows truly whence this great creation sprang?*

*Who knows whence this creation had its origin?*
*He, whether He fashioned it or whether He did not,*
*He, who surveys it all from the highest heaven,*
*He knows —or maybe even He does not know.*

*— Rig Veda, X.1291*

‘Maybe even He does not know!’ I love a faith that raises such a fundamental question about no less a Supreme Being than the Creator of the Universe Himself. Maybe He does not know, indeed. Who are we mere mortals to claim a knowledge of which even He cannot be certain?

---

Some speak simplistically of the holy trinity of Brahma, Vishnu and Shiva as three separate gods, 
but all three are in fact three sides of one complex being: aspects of the uttama purusha, the perfect personality. 
There is a God who created the gods (and the demons): Prajapati, Lord of all Creatures. 
There are multiple further manifestations of the divine in popular Hindu iconography: a myriad of gods and goddesses. 
This might well have resulted from the Vedic civilisation’s absorption of the tribal and folk deities it found being worshipped in India 
before its advance through the country; each local manifestation of a god or a goddess was included in the 
all-embracing Hindu pantheon, so that as Hinduism spread, it accommodated earlier forms of worship rather than overthrowing them. 
Similarly, many tribes revered animals, but instead of disrespecting them, Hinduism absorbed the non-humans too, by 
making them the companions or vehicles (vahanas) of the gods. So Vedic gods found themselves riding a lion, 
mounting a peacock, reclining on a swan, or sitting on a bull. 
It was part of the agglomerative nature of Hinduism that it neither rejected nor dismissed the faiths it encountered, 
but sought to bring them into the fold in this way.

---

In his historic speech at Chicago’s Parliament of the World’s Religions on 11 September 1893, Swami Vivekananda spoke of Hinduism as teaching the world not just tolerance but acceptance. The Swami believed that Hinduism, with its openness, its respect for variety, its acceptance of all other faiths, was one religion that should be able to spread its influence without threatening others. At the Chicago Parliament, he articulated the liberal humanism that lies at the heart of his (and my) creed: ‘I am proud to belong to a religion which has taught the world both tolerance and universal acceptance. We believe not only in universal toleration, but we accept all religions as true.
He went on to quote a hymn, the Shiva Mahimna Stotram, which he remembered from his formative years at school: ‘As the different streams having their sources in different places all mingle their water in the sea, so, O Lord, the different paths which men take through different tendencies, various though they appear, crooked or straight, all lead to Thee…the wonderful doctrine preached in the [Bhagavad] Gita echoes the same idea, saying: “Whosoever comes to Me, through whatsoever form, I reach him; all men are struggling through paths which in the end lead to me”.
This was a profoundly important idea, and central to the philosophy Vivekananda was preaching. Tolerance, after all, implies that you have the truth, but will generously indulge another who does not; you will, in an act of tolerance, allow him the right to be wrong. Acceptance, on the other hand, implies that you have a truth but the other person may also have a truth; that you accept his truth and respect it, while expecting him to respect (and accept) your truth in turn. This practice of acceptance of difference—the idea that other ways of being and believing are equally valid—is central to Hinduism and the basis for India’s democratic culture.
