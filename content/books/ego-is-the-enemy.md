---
title: "Ego Is the Enemy"
date: 2022-08-02T21:17:40+04:00
draft: False
tags: [ 'books', 'quotes']
categories: [ 'books']
---

Pick up a book on a topic you know next to nothing about. 
Put yourself in rooms where you're the least knowledgeable person. 
That uncomfortable feeling, that defensiveness that you feel when your most deeply 
held assumptions are challenged — what about subjecting yourself to it deliberately? 
Change your mind. Change your surroundings… An amateur is defensive. 
The professional finds learning (and even, occasionally, being shown up) to be enjoyable; they like 
being challenged and humbled, and engage in education 
as an ongoing and endless process.

- Ego Is the Enemy
  Ryan Holiday; 2016
