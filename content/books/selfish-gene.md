---
title: "Selfish Gene"
date: 2023-05-19T20:16:48+04:00
categories: [ 'Books' ]
tags: [ '' ]
draft: true
---

Intelligent life on a planet comes of age when it first works out the reason for its own existence. If superior creatures from space ever visit earth, the first question they will ask, in order to assess the level of our civilization, is: 'Have they discovered evolution yet?' Living organisms had on earth, without ever knowing why, for over three thousand million years before the truth finally dawned on one of them. His name was Charles Darwin. To be fair, others had had inklings of the truth, but it was Darwin who first put together a coherent and tenable account of why we exist. Darwin made it possible for us to give a sensible answer to the curious child whose question heads this chapter. We no longer have to resort to superstition when faced with the deep problems: Is there a meaning to life? What are we for? What is man? After posing the last of these the eminent zoologist G. G. Simpson put it thus: 'The point I want to make now is that all attempts to answer that question before 1859 are worthless and that we will be better off if we ignore them completely.'


The Selfish Gene
Richard Dawkins; 1976


