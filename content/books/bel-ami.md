---
title: "Bel Ami"
date: 2022-08-27T19:12:14+04:00
tags: ['books']
categories: ['books']
hidden: true
draft: True
---

But you'll come to realize, too, the dreadful anguish of despair. 

You'll find yourself struggling wildly in the stifling grip of doubt. 
You'll shout for help in every direction and no one will answer. 
You'll hold out your arms and beg to be rescued and loved and saved; but no one will come. 

Why do we have to suffer like this? 
No doubt the truth is that we were born to live more materialistically and less spiritually; but through 
too much thought we've created a discrepancy between our overdeveloped intelligence and the unchanging conditions of our life. 
Take the average man. Unless he's actually affected by personal disaster he's quite happy and doesn't suffer from all the unhappiness of others. 
Animals don't feel it either.

- Bel-Ami
  Guy de Maupassant, Douglas Parmée; 1885


