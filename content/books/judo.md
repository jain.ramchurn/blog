---
title: "Judo: History, Theory, Practice"
date: 2022-10-14T12:52:36+04:00
categories: [ 'books' ]
tags: [ 'books', 'quotes' ]
draft: false
---

The meaning behind the Kodokan's motto—"The Most Efficient Application of Strength," as Jigoro Kano put it—consists of the following. Assume that a person's strength is measured in some sort of units. An opponent's strength is ten, yours is seven. If your opponent pushes you with all his might, you won't be able remain standing, even if you resist with all your might. You'll either fall or step back. This is opposing strength with strength. But if you give way to the extent that you are pushed, maintaining your balance and withdrawing your body, your opponent will stagger forward and lose balance. At that moment, weakened by this awkward position, your opponent is very vulnerable. You, on the other hand, by maintaining your balance, can use all seven units of your strength, to score a victory.

"Give way in order to conquer." The highest degree of judo mastery is the ability to give way in the name of victory. Let's illustrate this tenet with the following example. Imagine you are a door and that you are locked shut; your opponent, however, is determined to break you open with his shoulder. If your opponent is big and strong enough and rams through the door (that is, you) from a running start, he will achieve his aim. Now let's consider a different turn of events. Instead of digging in your heels and resisting your opponent's onslaught, you time it so that when your opponent is just about to hit the door, you unlatch it. Then, not meeting any resistance and unable to stop, your opponent bursts through the wide open door, losing balance and falling. If at this time you raise the threshold slightly, "stick out your heel," so that he trips (if we're going to fantasize, let's do it right), then victory will be unconditional. Minimum effort, maximum effect.

Similarly, you can play with the lock when the door opens outward and your opponent pulls the door (that is, you) open. As soon as your opponent tries to jerk the handle, thinking he is going to break down the locked door, the lock is opened and he is sure to lose balance. If at this time we are able to take the handle off the door, as it were, then that person, who was counting on brute strength but met no resistance while groping with the void, will surely fall on his back. 



