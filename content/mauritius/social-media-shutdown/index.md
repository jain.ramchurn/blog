---
title: "Social Media shutdown in Mauritius"
date: 2024-11-10T15:28:21+04:00
categories: [ 'Mauritius' ]
tags: [ 'Social Media' ]
draft: false
---

On November 1, 2024, the Mauritian population was stunned to discover that the government had imposed a partial internet shutdown amid the electoral campaign. The ICT Authority (ICTA) instructed all internet service providers to block access to social media platforms until November 11, after the general election. The move was officially justified on the grounds of national security. 

![ICTA Communication](./icta_coommunique.jpg)

Later on, the Prime Minister's Office (PMO) Communication Cell released a press citing that the national security concerns arises from leaked audio recordings that allegedly compromised the integrity of the Republic and its international partnerships. According to the PMO, the recordings posed a "real risk" to national stability, prompting the Information and Communication Technologies Authority (ICTA) to direct internet service providers to restrict access to widely used social networks. A crisis committee was reportedly convened to address the risks and ensure a prompt return to normalcy

![PMO's press release](./pmo-pressrelease.jpg)

Despite these explanations, the decision was met with widespread criticism. [Ish Sookun](https://x.com/IshSookun) reached out to [international organizations](https://x.com/IshSookun/status/1852341925883363627) to raise awareness about the situation in Mauritius to which AccessNow has [responded](https://www.accessnow.org/press-release/keepiton-mauritius-end-crackdown-on-social-media/).


## The Recordings and Missie Moustass

Central to the social media shutdown was the controversy surrounding recordings leaked by an anonymous figure known as "Missie Moustass." "Missie Moustass" made his first appearance on Facebook and TikTok on October 18, 2024, claiming to be an ordinary civil servant who had worked in a secret government unit. He declared that he could no longer remain silent about the corruption he had witnessed and sought to reveal the truth by exposing prominent figures. He also alleged that citizens had been subject to phone tapping.

Within hours, the Facebook page gained over 1,000 followers, but by October 20, it was disabled, and the TikTok account was banned shortly after. Undeterred, Missie Moustass transitioned to YouTube, where he left a lasting [digital footprint](https://youtube.com/@MissieMoustass). The videos were skillfully edited, featuring introductions and images of the individuals involved. They were released episodically, structured as a 10-season series, with teasers for upcoming installments. Each video lasted about two minutes.

The authenticity of the recordings was later confirmed by some of the individuals featured. However, law enforcement issued warnings against the use of artificial intelligence, hinting at potential manipulations. Despite this, Missie Moustass quickly became one of the most recognizable names in Mauritius, though his identity remains shrouded in mystery.

For a detailed timeline and comprehensive coverage of the events, consult this [article from L’Express](https://lexpress.mu/s/revelations-de-missie-moustass-ebouriffant-scandale-539376).

## The Rise of VPNs and Tor

With social media platforms inaccessible, many Mauritians turned to virtual private networks (VPNs) and the Tor network to bypass restrictions and stay updated on Missie Moustass’s revelations. This surge in alternative internet usage underscored the growing concerns about freedom of expression and access to information during critical political moments.

The Tor Project's user metrics showed a notable increase in connections from Mauritius during the shutdown period.

![TOR metrics](./tor-metrics.png)

For those seeking guidance, Alex Bissessur’s blog post provided a detailed tutorial on how to [use Tor](https://alexbissessur.dev/use-tor) to circumvent censorship.

## Lifting the Ban

The social media blackout lasted for 35 hours. On November 2, 2024, the government and ICTA reversed their decision, and access to social media platforms was fully restored. While the sudden lifting of the ban brought relief, it also raised questions about the proportionality and motivations behind the suspension.

The move to block social media during a sensitive electoral campaign, especially in the wake of Missie Moustass's revelations, continues to spark debates about censorship, transparency, and democratic values in Mauritius.

For further information on the internet shutdown and its impact, refer to the [Internet Society’s report](https://pulse.internetsociety.org/shutdowns/mauritius-orders-blocking-of-social-media-sites-in-advance-of-election).


## Questions of Rights and Governance

This sequence of events raises pressing questions: Does the suspension of social media platforms violate the constitutional rights to freedom of speech and access to information? What role does the judiciary play in safeguarding these freedoms against executive overreach? How do governments balance national security concerns with the public's right to digital connectivity? The case of Mauritius in November 2024 highlights the tension between governance, technology, and individual rights in an era of rapid digital transformation.

Moreover, the controversy sheds light on the growing role of whistleblowers like Missie Moustass in exposing institutional malpractices. As Mauritius grapples with these complex challenges, it must navigate a path that upholds its democratic ideals while addressing legitimate security concerns.



#### References
* [VPN Demand Surges Around the World](https://www.top10vpn.com/research/vpn-demand-statistics/)
* [Mauritius blocked social media ahead of 2024 general election](https://explorer.ooni.org/findings/73729350400)
