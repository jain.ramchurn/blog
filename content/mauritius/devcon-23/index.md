---
title: "Developer Conference 2023"
date: 2023-07-23T15:28:21+04:00
categories: [ 'Mauritius' ]
tags: [ 'DevCon', 'MSCC' ]
draft: false
---

This year was a remarkable milestone for me as I had the privilege of being a speaker at DevConMU. I was ecstatic to have not just one, but two and a half sessions to share my knowledge and experiences with the audience. Allow me to explain the intriguing "half session" later on.

My journey with DevCon began back in [2019](https://www.mscc.mu/press-5th-developers-conference/), its fifth edition, at Voila Hotel & Flying Dodo Brewery, Moka and the theme was inspired by "Game of Thrones." [The event](https://sysadmin-journal.com/devcon-2019-in-a-nutshell/) left a lasting impact on me, igniting a passion to become a future speaker at such conferences. During that memorable time, I had the pleasure of meeting Ish Sookun, who delivered an engaging talk on btrfs. What made it even more memorable was that the talk took place in a casual setting around a table with beers on the first floor of Flying Dodo. It was an unexpected and refreshing approach to knowledge sharing.

In a coincidental twist, during my conversation with Ish, I confided that I was on the lookout for a Sysadmin job as I was in my *final* year of university. However, I learnt that the recruitment at LaSentinelle had already concluded. Nonetheless, destiny had other plans for me, as I had an interview scheduled at OceanDBA the following Monday for a Linux SysAdmin position. When I shared this with Ish, he mentioned that he knew Ronny and Joffrey, who were associated with OceanDBA. Ronny, being Ish's mentor, and Joffrey, the CEO, were key figures there.

As if orchestrated by fate, Joffrey happened to enter the room at that very moment, and Ish enthusiastically introduced me as his future interview candidate. The encounter left the best and first impression I could made, as I was interacting with one of the best technicians on the island during one of the greatest conferences in Mauritius.


Returning to this year's DevConMU, the 8th edition of the conference held extra significance as it commemorated the 10th anniversary of MSCC, the driving force behind the biggest tech event in Mauritius. The grand venue for the occasion was the prestigious Caudan Art Center, in the heart of Port Louis. The center boasted four rooms: (1)Andromeda named after the galaxy, (2) Titan, named after Saturn's largest moon, and (3) Pandora, and  (4) Tatooine, two fictitious planets. Among them, Andromeda took center stage as the largest room, boasting a proscenium setup.

## Day 1
The excitement began on the first day with attendees streaming in for registration. Speakers like me were greeted with special goodies as a token of appreciation. Participants were also handed tickets, allowing them to partake in the engaging games and activities hosted by various companies at their booths. A raffle at the end of the conference further added to the anticipation.

![devcon-badge](./img/badge.jpeg#c)

**My speaker badge.**

With registration done, the event kicked off with the grand Opening Ceremony in the Andromeda room. The atmosphere was electric, with professionals and students alike eagerly awaiting the insightful sessions ahead. Finding a spot toward the upstage, my friends and I were treated to a fantastic view of the proceedings.

![](./img/upper-stage.jpg#c)
**Dereck and Sandeep enjoying the view from the top.**

The first session I attended was the thought-provoking "Boosting Python with Rust" by the brilliant [Abdur-Rahmaan Janhangeer](https://twitter.com/osdotsystem). It set the tone for the day and left me excited for what was to come. Unfortunately, I had to miss the next session on "LaTeX/Beamer tips: How to customize your LaTeX/Beamer projects" by [Pritvi Jheengut](https://twitter.com/zcoldplayer) as hunger led me to seek out lunch. This year, lunchtime wasn't fixed to accommodate the varying preferences of attendees. 

I  had the pleasure of running into [Vidush Namah](https://twitter.com/VHNamah), and we decided to have lunch together. As we sat down, we engaged in a delightful conversation. Vidush, being a freelancer, shared some valuable insights about his work and offered me advice for my upcoming sessions as a speaker. I introduced him to i3wm and showed him my slides as our sessions were scheduled at the same time on the following day. Vidush is an amazing and cool guy to hang out.

In the afternoon, I attended "Astro to the Moon!" delivered by the founder of Mauritius Frontend Coders and Upcode.mu, [Sandeep Ramgolam](https://twitter.com/__Sun__). His demo on the framework Astro inspired me to consider rewriting my blog using it. Fun fact, Sandeep had forgotten to push his codes on Github for his presentation from his desktop the previous day, and he had to rewrite his codes again before and during his session. Next on my agenda was "Exploring Qwik, the resumable Javascript framework" by [Saamiyah Peerun](https://twitter.com/smearthelove). Her talk was enriched with hand-drawn illustrations, and she even surprised me with a seamless demo by cleverly using a video on her Google Docs. Thanks to the *qwik* pace of her talk, I managed to squeeze in "Implementing Generative AI and GPT Today," where I had a delightful chat with the speaker later on.


![sandeep](./img/sandeep.jpeg#c)
**Sandeep doing live-coding.**

![Saamiyah](./img/saamiyah.jpeg#c)
**Saamiyah introducing herself.**


Later on, I attended "Single Board Computer Clusters for Dummies" presented by the young and witty [Alex Bissessur](https://twitter.com/Alex_with_a_B). Despite his young age, he impressed the audience with his fluency and broad knowledge. The last session of the day was "AWS CI/CD Pipeline with Terraform" by [Chittesh Sham](https://twitter.com/tesh1224). Chittesh made his [presentation highly interactive, inviting my friend and me to play the roles of a client and engineer respectively](https://youtu.be/ZFUXJT9U3sU?t=30), which made the experience all the more engaging.

![Alex](./img/alex.jpeg#c)
**Alex talking on blue/green deployment among k8s, raspberry pi, and clusters.**

The day concluded with a lively networking hour at the Suffren Hotel, generously sponsored by Spoon Consulting. Amidst the vibrant atmosphere, I had a fantastic time with friends, connecting with fellow attendees, and sharing experiences.

## Day 2

The next day, I arrived at the Caudan Art Center a little early, accompanied by my cousin,[Yash](https://www.linkedin.com/in/karmajeet-yashraj-sharma-sukon-943ba2190/) who graciously offered to help record my sessions and take some photographs. After assisting him with the registration process, I caught up with some friends. We then proceeded to set up the camera for recording, which unfortunately meant missing out on the morning talks.

My session, titled "i3wm: a Beginner's Guide," was scheduled for 11 am in the Tatooine room. Though my audience was relatively small, I found comfort in seeing some familiar faces among them, which helped ease my nervousness. To kick off my talk, I began with a quick poll to gauge the number of Linux users in the audience, and to my pleasant surprise and delight, nearly everyone raised their hands. Encouraged by their enthusiasm, I delved into the basics of the i3 window manager and then explored its details and workflow. However, during my live demo, I faced some technical issues with the projector. Looking back, I realized that a simple solution would have been to disable the monitor on my laptop and make the projector the main output. Despite the hiccup, the audience was incredibly understanding, and they laughed at the subtle jokes about Linux distro and Vim. I genuinely hope that I was able to effectively share my knowledge and that someone in that room felt inspired to try out i3 or another window manager.

![](./img/i3wm.jpg#c)
**I was looking for my workspaces to show the stack list based approach.**

Following my talk, I had a brief meeting with Ish, Dereck, and Shravan to discuss our upcoming 2-hour session on the Linux bootcamp. We designed the bootcamp in a way that allowed attendees to join at any time and follow the topics of their interest. The room was overflowing with participants, and we even had to request additional chairs to accommodate everyone. It was heartwarming to see the attendees so engrossed in the talks, and surprisingly, almost everyone stayed till the end of the session. Due to time constraints, Ish had to combine and skip some topics, and my topic became a little offset, but we still managed to deliver a successful bootcamp. I was thrilled with the engagement and interest we received, and it was encouraging to see the audience eager to learn about Linux.

![](./img/agenda-bootcamp.png#c)
**The bootcamp agenda.**

![](./img/overflowing-room#c)
**Room packed with attentive attendees.**


Following the bootcamp, we swiftly transitioned into our next talk, "Improving Linux Updates in Mauritius." Although [Girish Luckhun](https://twitter.com/sudo_gluck) was initially slated to be my co-speaker, he couldn't make it, and Ish graciously stepped in as a replacement. I thoroughly enjoyed discussing my contributions, and the synergy between Ish and me was great. The primary goal of the talk was to attract more volunteers and sponsors to support Linux development in Mauritius, and I felt confident that we had achieved that objective.
![](./img/all-linux-mirrors.jpeg#c)
**Showing all linux mirrors in Mauritius**

As the day progressed, I had initially planned to attend other sessions, but I found myself feeling both physically tired and quite hungry, so I had to forego those plans.

However, there was one event — the Entertainment Hour by Fake Artist Ltd. It turned out to be an entertaining and captivating experience that provided the perfect opportunity to unwind. 

## Day 3



As the final day of DevConMU dawned, I decided to take it easy and have a more relaxed experience. I began my day by attending the session on "The Power of Knowledge Graph". Next, my eloquent friend, [Houzair Koussa](https://www.linkedin.com/in/houzairk/), delivered an engaging session titled "Philosophy Meets Code". Following that, I joined the talk on "Right to Repair Much?: with Reeaz of OneoONE" by Loic Forget. The unique format, resembling a podcast, made it an interesting and informative discussion. Loic and Reeaz openly discussed the state of repairs on the island, shedding light on how this industry operates. By the time the session concluded, it was already past 1 o'clock, and I decided to head to Panarottis for lunch with Dereck, Noor, and another close friend, Sarvesh.

While our lunch was enjoyable, I regretfully missed the sessions on "Dev Containers: What are they and why do you need them?" and "Website from Zero to Production in 10 mins (frontend)". I heard that these sessions were well-attended.

Returning to the Caudan Art Center, I joined the Lightning Talks organized by Sandeep. These short and concise sessions included various topics like "10 Tips to make your website more accessible" by Kushul Soomaree, "Why should we even care using the Mauritius Internet Exchange Point (MIXP)?" by Keessun Fokeerah, "Instant App Development" by Jeshta Bhoyedhur, "Line Endings" by Danshil Kokil Mungur, "Debugging" by Chittesh Sham, and "What's new in CSS" by Sandeep Ramgolam. These lightning talks were packed with valuable insights, and as always, Chittesh added his humor and entertainment, keeping the audience engaged.

Houzair and I shared the responsibility of time management during the Lightning Talks, ensuring that speakers adhered to their time limits. Holding up the laptop with a countdown timer became our way of ensuring that speakers respect their time limit. 

The day culminated with the much-anticipated closing ceremony, where it was announced that there were an impressive 1502 attendees. The popularity of the event had even caused MSCC to run out of badges and lanyards, resorting to ink stamps to check-in attendees. The games were organized more efficiently this time, and the abundance of gifts and goodies added to the excitement. The sponsors' competitions yielded winners, and everyone eagerly awaited the raffle draws, hoping for their number to be called. I personally wished there were even more raffle draws, as it always adds an element of thrill and surprise to the event.

On a personal note, I was delighted to have won a prize for answering a question during the conference.

![](./img/prize-won.jpg#c)


With the closing ceremony, DevConMU came to an end, leaving me with a wealth of knowledge, incredible experiences, and lasting connections within the tech community. I would like to thank MSCC, [Jochen](https://twitter.com/JKirstaetter) and Ish so this opportunity and I look forward to the next edition with anticipation and gratitude for being part of such an enriching conference. 

And the next year theme is RETRO!

![](./img/retro-theme.png#c)


Here are the some blogs about DevCon 2023:

[Alex's post on DevCon](https://xelab04.github.io/activities/devcon23)
[Developers Conference 2023: It’s a wrap by Arshad Pooloo](http://arshadpooloo.com/developers-conference-2023-its-a-wrap/)

The [`#devconmu`](https://twitter.com/hashtag/devconmu) hashtag was trending on twitter.

---

Here's some pictures taken at the devcon.
If there's anything that shouldn't be present, kindly inform me, and I will promptly remove it.

![](./img/brief-meeting.jpeg#c)
**A brief meeting!**

![](./img/ish-showing-how-to-install-postman-gui.jpeg#c)
**Ish was installing postman using the software app center**

![](./img/dereck-systemd.jpg#c)
**Dereck was about to start his talk on systemd.**

![](./img/shravan-with-footwear.jpeg#c)
**Shravan with his famous *savatte*.**

![](./img/jain-talking-on-containers.jpeg#c)
**I talked on containers.**

![](./img/ish--starting-linux-mirror.jpg#c)
**Ish took the lead!**

![](./img/ish-talking-sponsors.jpg#c)
**Ish was talking about sponsors for the mirrors.**

![](./img/trio.jpg#c)
**Tired faces tried to get a good photo.**

![](./img/loic-rezea.jpeg#c)
**Loïc’s presented on Right to Repair, with Reaz as guest.**

![](./img/houzair.jpeg#c)
**Philosophy meets Code by Houzair**

![](./img/fake-artist.jpg#c)
**The Fake Artist's performance.**

![](./img/group.jpg#c)
**A nice group photo!**

![](./img/backstage.jpg#c)
**Dereck after the closing ceremony!**

![](./img/sharing-a-drink.jpg#c)
**Shared a drink with Vidush and Pritvi. Cheers!**


