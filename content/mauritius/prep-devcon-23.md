---
title: "Three Tips for Attending Developers Conference 2023"
date: 2023-07-10T12:05:58+04:00
categories: [ 'Mauritius' ]
tags: [ 'devcon' ]
draft: False
---

This year marks the 10th anniversary of MSCC and the 7th edition of Developers Conference. It will take place for 3 days, from 20th to 22nd July at the Caudan Arts Centre. There will be [4 rooms](https://conference.mscc.mu/agenda): Andromeda, Pandora, Titan and Tatooine which will have around 30 talks. It's a huge conference with a ton of cool and interesting things to check out.

Start your day off right by registering in the morning at the Andromeda room. 

## Tip #1: Plan ahead

Take the time to curate a personalized schedule by identifying the talks and sessions that align with your interests. Remember, it's your time, so don't hesitate to leave a session if it fails to captivate you. Prioritize what matters most to you.

## Tip #2: Take breaks

Devcon is a marathon, not a sprint. There is a lot of events and content packed into the three days you are there. Trying to consume it all at once without taking a breath is going to wear you out.
Avoid overwhelming your schedule with back-to-back sessions. Instead, leave gaps between sessions to reflect and absorb the knowledge you've gained. Embrace the opportunity to step outside, breathe in the fresh air, and bask in the natural light. Taking breaks will rejuvenate your mind and enhance your overall experience.

## Tip #3: Enjoy the space to focus on learning

Devcon offers a unique space for personal growth and discovery. Let yourself fully indulge in the experience of expanding your knowledge and skills. You never know what exciting developments may arise. Perhaps you'll leave the conference with a revolutionary architectural concept or forge valuable connections that open unexpected doors. Embrace the abundance of new ideas and stay open to the possibilities.


## Conclusion

Remember to pace yourself, plan for the things you don't want to miss, and to give yourself the space to let things soak in.


I will be speaking at Devcon in two sessions: [A Beginner's Guide to the i3 Window Manager](https://conference.mscc.mu/agenda/438487) and [Improving Linux Updates in Mauritius](https://conference.mscc.mu/agenda/454708). Additionally, I am excited to participate in the [Linux Administration Bootcamp](https://conference.mscc.mu/agenda/454720), where I will delve into the fascinating topics of containers and automation. Join me for these insightful sessions and let's explore the future of Linux together!
