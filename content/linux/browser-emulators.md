---
title: "Emulators in browser"
date: 2022-08-01T20:54:20+04:00
tags: ['OS', 'emulators']
categories: [ 'linux']
draft: False
---

## Virtual x86

[Virtual x86](https://copy.sh/v86/) emulates an x86-compatible CPU and hardware. It is written mainly in Rust,Javascript and C - source code can be found [here](https://github.com/copy/v86).

The following OS are supported:
* Arch Linux 
* Damn Small Linux 
* Buildroot Linux 
* ReactOS 
* Windows 2000 
* Windows 98 
* Windows 95 
* Windows 1.01 
* MSDOS 
* FreeDOS 
* FreeBSD 
* OpenBSD 
* 9front 
* Haiku 
* Oberon 
* KolibriOS 
* QNX

## JSLinux

[JSLinux](https://bellard.org/jslinux/) which enable you to run Linux or other operating system in your browser. The emulator is based upon on [TinyEMU](https://bellard.org/tinyemu/)  You can learn about the techinical implementation [here](https://bellard.org/jslinux/tech.html).

Available OS are: Alpine Linux, Windows 2000, FreeDOS, Buildroot and  Fedora 33. 

