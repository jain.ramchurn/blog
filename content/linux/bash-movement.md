---
title: "Bash Shortcut: Ctrl and Alt keys"
date: 2022-08-01T21:30:17+04:00
tags: [ 'bash', 'cli' ]
categories: [ 'linux']
draft: false
---
> ### Commands for moving

```
Ctrl + a: move to the beginning of line
```
```
Ctrl + e: move to end of line
```
```
Ctrl + f: Move forward a character
```
```
Ctrl + b: Move back a character
```
```
Alt + f: Move forward to the end of the next word
```
```
Alt + b: Move back to the start of the current of previsous word
```
---

> ### Commands for Manipulating the History

```
Ctrl + n: Fetch the next command form the history list (same as Down arrow)
```
```
Ctrl + p: Fetch the previous command form the history list (same as Up arrow)
```
```
Alt + <:  Move to the first line in the history
```
```
ALt + >: Move to the end of the input history ( the line currently bein entered)
```
```
Ctrl + r: begins a backward search through command history
```
```
Alt + p: Search backward through the history using a non-incremental search for a string supplied by the usser
```
```
Alt + n: Search forward through the history using a non-incremental search for a string supplied by the usser
```
```
Alt + . : Insert the last word of the previous history entry
```
```
Ctrl + x + Ctrl + e: launch editor defined by $EDITOR to input your command. Useful for multi-line commands.
```
---
> ### Commands for Changing text

```
Ctrl + d: deletes the character under the cursor
```
```
Ctrl + v + TAB: insert a tab character
```
```
Ctrl + t: transpose the character before the cursor with the one under the cursor, press Esc + t to transpose the two words before the cursor
```
```
Alt + t: transpose the two words before the cursor
```
```
Alt + u: uppercase the current (or following) word
```
```
Alt + l: lowercase the current (or following) word
```
```
Alt + c: capitalize the current (or following) word
```
```
Ctrl + _: undo the typing
```

---
> ### killing and yanking

```
Ctrl + k: deletes all tet from the cursor to the end of line
```
```
Ctrl + w: cut the word before the cursor; then Ctrl +y to yank it
```
```
Ctrl + u: cut the line before the cursor; then Ctrl +y to yank it
```
---

> ### Others
```
Ctrl + l: equivalent to clear
```

```
Ctrl + s: to stop output to terminal
```
```
Ctrl + q: to resume output to a terminal after Ctrl + s

