---
title: "/usr Merge"
date: 2023-05-20T17:56:03+04:00
categories: [ 'Linux' ]
tags: [ 'linux' ]
draft: false
---

While browsing lwn.net, I came across this [article](https://lwn.net/Articles/932158/) mentioning Debian's decision to delay the implementation of the `/usr` merge.
The article prompted me to dive deeper into the topic and explore its significance.

## What is /usr merge?

The /usr merge (also known as /usr move) entails relocating specific directories, including /bin, /sbin, and /lib from the root file system ("/") to the /usr directory. This restructuring aims to streamline the system architecture and improve versatility.

To grasp the importance of the /usr merge, it is essential to examine its historical origins. The separation of the /usr directory from the root file system was initially implemented in the 1980s as a solution to optimize disk space usage. Back then, computer storage was relatively limited, and seperating user-related data from core system files proved beneficial. This helped in minimizing the size of the / partition, enabling the system to boot seamlessly from a single drive and to mount the /usr partition after start-up. 

However, as time passed, the distinction between the root file system and the /usr directory became more intricate. While some critical directories like /bin and /sbin remained in the root file system, others gradually migrated to the /usr directory. This fragmented structure introduced complexities in system management and posed potential risks during system upgrades.

## Benefits
The advantages of the /usr merge include:
* Simplified Script/Program Compatibility: When scripts or programs written for other Unix or Linux distributions are ported to your specific distribution, there is no longer a need to modify file system paths for the called binaries. This eliminates a common source of frustration, and /usr/bin and /bin (or /usr/sbin and /sbin) become completely interchangeable.
* Enhanced Compatibility with Other Unix Systems: The /usr merge improves compatibility with other Unix systems, particularly Solaris, in terms of appearance and overall functionality.
* Better Compatibility with GNU Build Systems: The /usr merge enhances compatibility with GNU build systems, making it easier to integrate and build software projects using these systems.
* Improved Alignment with Upstream Development: By adopting the /usr merge, the distribution becomes more compatible with current upstream development efforts, ensuring a smoother integration of updates and new features.

## Implementation amongst distribution
Curious about the current status of the /usr merge of different distribution.


### Fedora

Fedora was the second to implement the /usr merge after Oracle Solaris and the first Linux distribution to make the move [2012](https://fedoraproject.org/wiki/Features/UsrMove).

### Arch Linux

```
$ ls -l /bin /sbin /lib /lib64 
lrwxrwxrwx 1 root root 7 Feb  1 00:51 /bin -> usr/bin
lrwxrwxrwx 1 root root 7 Feb  1 00:51 /lib -> usr/lib
lrwxrwxrwx 1 root root 7 Feb  1 00:51 /lib64 -> usr/lib
lrwxrwxrwx 1 root root 7 Feb  1 00:51 /sbin -> usr/bin
```
/bin, /sbin, and /usr/sbin are symbolic links to /usr/bin, and /lib and /lib64 are symbolic links to /usr/lib. [Here](https://lists.archlinux.org/pipermail/arch-dev-public/2013-May/025021.html) discussing the /usr move.


### Opensuse

[UsrMerge is active in openSUSE Tumbleweed since 20210527](https://en.opensuse.org/openSUSE:Usr_merge)


### Gentoo

The /usr merge is optional in [Gentoo](https://gitweb.gentoo.org/proj/baselayout.git/commit/?id=77e9c6f275399cdef64d432a19da51bc9c9cd0bf). There is a [script merge-user](https://wiki.gentoo.org/wiki/Merge-usr) which perform the migration.

### Other distrubutions

Other distribution which has /usr merge by default:

* Mageia
* Red Hat Enterprise Linux
* Chakra
* Void Linux
* Debian
* Ubuntu

## Further Reading

[The Case for the UsrMerge](https://www.freedesktop.org/wiki/Software/systemd/TheCaseForTheUsrMerge/)

## References
http://lists.busybox.net/pipermail/busybox/2010-December/074114.html
https://lwn.net/Articles/932158/
https://lwn.net/Articles/890219/
https://salsa.debian.org/md/usrmerge/raw/master/debian/README.Debian
https://linux.fandom.com/wiki/Usr_move
