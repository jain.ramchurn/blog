---
title: "Console, Terminal, virtual terminal"
date: 2022-08-09T19:21:57+04:00
tags: ['unix', 'linux', 'tty', 'console']
categories: [ 'linux']
draft: true
---

## A little history

In the early days of unix, you interacted with a computer which resemble a typewriter, also called a teletypewriter or "tty" in short.

![teletypewriter](https://images.wisegeek.com/teletypewriter.jpg)



---

## Resources

http://www.linusakesson.net/programming/tty/index.php
