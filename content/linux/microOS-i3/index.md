---
title: "Experimenting with OpenSUSE MicroOS and i3 window manager: Day 1"
date: 2023-06-13T15:59:02+04:00
categories: [ 'linux' ]
tags: [ 'i3','MicroOS' ]
draft: False
---
Immutable desktops have gained significant traction in recent years due to their stability and security advantages. If you're interested in exploring immutable distributions, you can refer to this comprehensive list on GitHub that provides a curated selection of options: https://github.com/castrojo/awesome-immutable#distributions.

Two distribution caught my attention: [Fedora Sericea](https://fedoraproject.org/sericea/) and [Greybeard](https://github.com/ProjectGreybeard/Info)(not on the list yet).  Fedora Sericea is a variant of Fedora Silverblue, while Greybeard is an experimental project based on openSUSE MicroOS Desktop. Both distributions are built on the Sway tiling window manager, which serves as a drop-in replacement for the i3 window manager for X11. 

Intrigued by the possibilities, I embarked on a little experiment and set up the i3 window manager on openSUSE MicroOS. Here's what I did:

First, I downloaded the openSUSE MicroOS ISO from https://en.opensuse.org/Portal:MicroOS/Downloads . Then, using Virtual Machine Manager, I created a virtual machine with the following specifications:
   * Disk: 20GB
   * RAM: 2GB
   * CPU: 2

With the virtual machine set up, I proceeded with the installation:

1. During the installation process, I chose the "MicroOS only" system role.
2. In the software selection step, I made sure to include the "X Window System" and "Fonts" packages.

![software-selection](software-selection.png)

Once the installation was complete, I started setting up the i3 window manager:

3. In the TTY, I ran the following commands to install i3 and the necessary packages. Reboot to activate the changes.
```
# transaction-update pkg install i3 i3blocks i3locks dmenu dunst
# reboot
# transactional-update pkg install lightdm dejavu-fonts
# reboot
```
4. After the system rebooted, I changed the target unit to "graphical" to enable the graphical login screen:

```
# systemctl isolate graphical.target
```
![lightdm-login](./lightdm_login.png)

See that the power management options are greyed out as the greeter needs additional configuration.


5. Upon logging in, the `i3-config-wizard` window appeared, allowing me to generate a configuration file for my user or use the default one. 

![i3-config-wizard](./i3-wizard-config.png)

6. I then configure the `i3bar` to use i3blocks instead of `i3status` and restarted i3 inplace by `mod+Shift+r`.

7. To ensure that the system boots into graphical mode by default, I ran the following command.

```
# systemctl set-default graphical.target
```

And voila! Here's how it looks.

![i3](i3_microOS.png)

In my next endeavor, I plan to expand the functionality of my setup by adding packages to cater to networking, audio, and other essential features. Additionally, I will integrate various utilities, applications, and tools to further enhance the overall user experience.
