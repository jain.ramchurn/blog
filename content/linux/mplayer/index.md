---
title: "Capture ASCII photo via mplayer"
date: 2022-08-31T20:30:13+04:00
categories: [ 'linux' ]
tags: [ 'terminal', 'ASCII', 'fun' ]
draft: False
---

## Mplayer 
Mplayer which stands for movie player for linux and supports most video and audio formats. Its manpage is really long. Check [this page](https://help.ubuntu.com/community/MPlayerTips) for a tips and tricks for mplayer and the [FAQ](https://wiki.multimedia.cx/index.php?title=MPlayer_FAQ) from multimedia.cx is a compilations of  questions and answers from the #mplayer IRC channel.

## Capture photo using mplayer into PNG file

You can capturing video and then convert the frame into png or other formats.

```
$ mplayer -vo png -frames 3 tv://
```

Let's break it down:

* `mplayer` - movie player
* `vo`      - Specify a priority list of video output drivers to be used
* `png`     - Output each frame into a PNG file in the current directory
* `frames`  - Play/convert only first frames, then quit.

The photo will be store in the current directory and saved as 0000000N.png.
You can specify  the directory to save the PNG files using `outdir` and  specify the prefix to be used for the PNG filenames using `prefix`.

![tigger](./img/tigger.png#c)

Here's a spooky photo of Tigger from Winnie the pooh and friends.


## ASCII-ART

Here's the fun part!
You can capture photo via the webcam into ASCII art using the [caca library](http://www.mplayerhq.hu/DOCS/HTML/en/caca.html) which is a colour ASCII Art library.


```
$ mplayer tv:// -vo caca -vf mirror -really-quiet setfont Uni1-VGA8
```

Break down:

* `-really-quiet` - Display even less output and status messages than with -quiet. 

* `-vf `          - Setup a chain of video filters.
* `mirror`        - Mirrors the image on the Y axis.
* `setfont`       - Setting font. The smaller the font – the better’s the output. 

---

Me in ASCII Art.
![Me in ASCII](./img/me.png#c)


My pal Vaibhav and I.
![Me and my pal in ASCII](./img/pal_and_me.png#c)

Squint your eyes to see the images clearly.

Have fun!

---

### Resources
https://www.systutorials.com/how-to-capture-camera-photo-video-using-mplayer-on-linux/

http://www.mplayerhq.hu/DOCS/man/en/mplayer.1.html
https://web.archive.org/web/20220831170631/http://andrusiv.com/blog/tag/mplayer.html

https://help.ubuntu.com/community/MPlayerTips
https://wiki.multimedia.cx/index.php?title=MPlayer_FAQ
