---
title: "Firefox with Tab Style Tree"
date: 2023-03-14T18:04:33+04:00
categories: [ 'Linux' ]
tags: [ 'firefox' ]
draft: False
---

As someone who often finds myself with multiple Firefox windows open and dozens of tabs across them, managing my browsing history can be challenging. I struggle to keep track of how I ended up on a particular page, as I frequently open links in new tabs without bookmarking them. As a result, the number of tabs I have open can quickly become overwhelming.

To tackle this issue, I started searching for a Firefox extension that would help me organize my tabs in a more hierarchical and structured way. That's when I stumbled upon [Tree Style Tab](https://github.com/piroor/treestyletab)(TST) and I found that it was an execellent fits for my browsing workflow. 

Another add-on which I came across is [Sidebery](https://github.com/piroor/treestyletab) and its wiki was quite helpful. More on that, later.


Back to TST. New tabs opened from the current tab are automatically organized as "children" of the current tab. his way, I can group related tabs together and collapse them with a single click. I can also restructure the tree via drag and drop, which makes it easy to move tabs between branches.

Another great aspect of TST is its sidebar panel, which can be toggled with the F1 key. The sidebar panel can be located on either the left or right side of the screen, depending on your preference. TST also provides an [API](https://github.com/piroor/treestyletab/wiki/API-for-other-addons) for other extensions, which can extend the behavior of the sidebar panel.

One extension that I found particularly helpful was [TST Tab Search](https://addons.mozilla.org/en-US/firefox/addon/tst-search/). This add-on allows me to search for tabs within TST's sidebar and quickly find and activate them. I no longer have to hunt through dozens of tabs to find the one I need. 

However, I did find the top horizontal tab bar to be redundant while using TST. To hide the bar, I followed the steps outlined in the [wiki of Sidebery](https://github.com/mbnuqw/sidebery/wiki/Firefox-Styles-Snippets-(via-userChrome.css)).

I create a directory `chrome` under my Firefox `Profile Directory` which is `/home/zain/.mozilla/firefox/rh8m6e00.default-release/chrome`. The Profile Directory can be obtained from `(Menu > Help > Troubleshooting Information > Profile Directory`. Under the `chrome` directory, create a file `userChrome.css` and add the following code:

```
#TabsToolbar {
   visibility: collapse;
}

#TabsToolbar {
  display: none; 
} 

```

After restarting Firefox, the top tab bar was no longer visible, and I could focus on navigating my tabs using TST's sidebar panel without losing my browsing trail.

This is how my firefox looks now.

![Firefox with TST](./img/firefix-TST.png#c)
