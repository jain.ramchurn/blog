---
title: "Neovim Config Switcher"
date: 2023-04-20T10:12:07+04:00
categories: [ 'linux' ]
tags: [ 'neovim' ]
draft: false 
---

[Neovim version 0.9.0](https://newreleases.io/project/github/neovim/neovim/release/v0.9.0) has been released and one of it's new [feature](https://github.com/neovim/neovim/pull/22128) is the introduction of the environmental variable `$NVIM_APPNAME`. 

Setting `$NVIM_APPNAME` makes Neovim look for its configuration directory in $XDG_CONFIG_HOME/$NVIM_APPNAME instead of $XDG_CONFIG_HOME/nvim.
1

Here's an example to give you a better idea.

We have the [NvChad](https://nvchad.com/)'s config.
```
$ ls .config/NvChad
init.lua  lazy-lock.json  LICENSE  lua
```

To make Neovim use this particular configuration file, we set the env variable `$NVIM_APPNAME` and open `nvim`.
```
NVIM_APPNAME=NvChad nvim
```

Instead of having to type the variable `$NVIM_APPNAME` again and again, you can use `alias` as follows:
```
alias nvim-chad="NVIM_APPNAME=NvChad nvim"
alias nvim-lazyvim="NVIM_APPNAME=lazyvim nvim"
alias nvim-lvim="NVIM_APPNAME=lvim nvim"
```

Youtuber [Elijah Manor](https://www.youtube.com/watch?v=LkHjJlSgKZY) took a step further by implementing a function to display drop-box to choose the config to use. [Here is the gist on github](https://gist.github.com/elijahmanor/b279553c0132bfad7eae23e34ceb593b)

He used zsh function together with fzf. Other users implmented the function in their favourite shell.

The function in bash and `C^n` binds to the launcher.

```
nvims() {
  items=("default" "NvChad" "lvim" "lazyvim")
  config=$(printf "%s\n" "${items[@]}" | fzf --prompt=" Neovim Config  " --height=~50% --layout=reverse --border --exit-0)
  if [[ -z $config ]]; then
    echo "Nothing selected"
    return 0
  elif [[ $config == "default" ]]; then
    config=""
  fi
  NVIM_APPNAME=$config nvim $@
}

bind -x '"\C-n": nvims'
```

Here's how it looks like:

![Neovim config switcher](./neovim-dropbox.png)

If you select nothing, it will default to neovim configuration.


