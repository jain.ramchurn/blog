---
title: "Inquisitive Insights: June 23"
date: 2023-07-01T20:21:49+04:00
categories: [ 'InquisitiveInsights' ]
tags: [ 'InquisitiveInsights' ]
draft: false 
---
Welcome back to another exciting edition of Inquisitive Insights! 
In this month's installment, I have handpicked an array of incredible discoveries and resources that will captivate your mind and propel you further along your tech journey.

## Awesome websites
* [cloudbite](https://cloudbite.attejuvonen.fi/?deck=aws&card=-688016922)
* [Brute.Fail: Watch brute force attacks fail in real time](https://brute.fail/)
* [fossfox: work in open-source & get paid for it](https://fossfox.com/)


## News
* [AWS Certified: Associate Challenge](https://pages.awscloud.com/GLOBAL-ln-GC-Traincert-Associate-Certification-Challenge-Terms-and-Conditions-2023-learn.html?sc_channel=sm&sc_campaign=AWS_Training_and_Certification&sc_publisher=LINKEDIN&sc_geo=GLOBAL&sc_outcome=adoption&sc_content=TRAINCERT-200-Associate-Cert-Challenge&trk=4fff061c-1cbe-4911-a0d3-c947ecbd04dc&linkId=218346381)
* [openSUSE MicroOS Desktop Gnome was renamed to openSUSE Aeon, and the Plasma Desktop version is being renamed to openSUSE Kalpa](https://en.opensuse.org/Portal:Kalpa)
* [Red Hat’s commitment to open source: A response to the git.centos.org changes ](https://www.redhat.com/en/blog/red-hats-commitment-open-source-response-gitcentosorg-changes)
* [Pixelfed Introducing Import from Instagram](https://pixelfed.blog/p/2023/feature/introducing-import-from-instagram)


## Tutorials
* [GUI development with Rust and GTK 4](https://gtk-rs.org/gtk4-rs/stable/latest/book/introduction.html)
* [Google Cloud: Generative AI learning path](https://www.cloudskillsboost.google/journeys/118)
* [learnaws.org](https://www.learnaws.org/archives/)
* [External Articles and Resources about Embedded Design](https://hardwareteams.com/docs/embedded/embedded-resources/)
* [Style your RSS feed](ihttps://darekkay.com/blog/rss-styling/)

## Tools
* [Modern UI for Ansible](https://www.ansible-semaphore.com/)
* [Wolfi: inux OS designed for the container and cloud-native era](https://github.com/wolfi-dev)
* [kaniko: tool to build container images from a Dockerfile](https://github.com/GoogleContainerTools/kaniko)
* [TailwindShades](https://www.tailwindshades.com/)
* [hurl](https://hurl.dev/)

## Awesome Repositories
* [The open guide to AWS](https://github.com/open-guides/og-aws)

## Awesome blog posts
* [Introducing Wolfi: The first Linux (un)distro designed for securing the software supply chain](https://www.chainguard.dev/unchained/introducing-wolfi-the-first-linux-un-distro)
* [Take your music offline](https://blog.orhun.dev/take-your-music-offline/)
* [Kubernetes Internals: Inside The Mind of A Monster](https://compileralchemy.substack.com/p/kubernetes-internals-inside-the-mind)
* [How does Linux really handle writes?](https://www.cyberdemon.org/2023/06/27/file-writes.html)
* [How to do great work](http://paulgraham.com/greatwork.html)
* [CLI tools hidden in the Python standard library](https://til.simonwillison.net/python/stdlib-cli-tools)
* [Rust fact vs. fiction: 5 Insights from Google's Rust journey in 2022 ](https://opensource.googleblog.com/2023/06/rust-fact-vs-fiction-5-insights-from-googles-rust-journey-2022.html)
* [Don't Skip 1 on 1s](https://blog.danielna.com/dont-skip-1-on-1s/)
* [Common bugs in writing](http://www.cs.columbia.edu/~hgs/etc/writing-bugs.html)
* [Decoding small QR codes by hand](http://blog.qartis.com/decoding-small-qr-codes-by-hand/)
* [My personal framework for flourishing](https://ljvmiranda921.github.io/life/2021/09/21/build-earn-play/)


## Videos
* [A cracking time: Exploring Firecracker & MicroVMs](https://www.youtube.com/watch?v=CYCsa5e2vqg)
* [Security Considerations for Container Runtimes (podman introduction)](https://www.youtube.com/watch?v=HIM0HwWLJ7g)
* [Why you should be running the MicroOS Desktop](https://www.youtube.com/watch?v=lKYLF1tA4Ik)
* [Huge Open Source Drama](https://www.youtube.com/watch?v=kF5pyVUQBH8)

## Books
* [The AWK Programming Language, Second Edition](https://awk.dev/)

Until next time, happy learning and stay curious!
