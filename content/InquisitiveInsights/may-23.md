---
title: "Inquisitive Insights: May 2023"
date: 2023-06-02T11:11:50+04:00
categories: [ 'InquisitiveInsights' ]
tags: [ 'InquisitiveInsights' ]
draft: false
---

Hey there, I  am starting a new monthly series called **Inquisitive Insights** which will be a gateway to the exciting world of tech. I'll be sharing the coolest and most intriguing discoveries which are related to my interests and career. Whether you're a coding whiz or a curious beginner, this series is designed to entertain, inform, and inspire you on your tech journey.

In each edition, we wil have a collection of awesome git repositories, stream of tech news, fascinating blog posts, and tutorials to sharpen your skills.

## Tools

* [Diataxis - a systematic framework for technical documentation authoring](https://diataxis.fr/)
* [Noogle - Nix API search engine that allows you to search functions based on their types and other attributes in nix projects.
](https://noogle.dev/) 

## Awesome Repositories
* [Reverse Interview](https://github.com/viraptor/reverse-interview)
* [teddit - A free and open source alternative Reddit front-end focused on privacy](https://codeberg.org/teddit/teddit)


## Tutorials
* [Learn x86-64 assembly by writing a GUI from scratch](https://gaultier.github.io/blog/x11_x64.html) 
* [Fast-terraform](https://github.com/omerbsezer/Fast-Terraform)


## Awesome Websites
* [degreeless.design](https://degreeless.design)
* [wiktok - A recommendation UI for Wikipedia](https://wiktok.org)
* [The Open Buddhist Univeristy](https://buddhistuniversity.net)
* [iHackaday.io is the world's largest collaborative hardware development community.](https://hackaday.io/discover)

## Awesome blog posts
* [Polar Night](https://brr.fyi/posts/polar-night)
* [Memory Allocation](https://samwho.dev/memory-allocation)
* [Dear Ubuntu](https://hackaday.com/2023/05/22/dear-ubuntu/)

## Books
* [Learn to Program with Assembly (2021) by Johnathan Bartlett](https://www.bartlettpublishing.com/site/books/learn-to-program-with-assembly/)
* [Programming From the Ground Up](https://download-mirror.savannah.gnu.org/releases/pgubook/ProgrammingGroundUp-1-0-booksize.pdf)

## News
* [Security.txt now mandatory for Dutch government websites](https://netherlands.postsen.com/trends/198695/Securitytxt-now-mandatory-for-Dutch-government-websites.html)
> The security.txt file on a web server contains the contact information for making contact if any vulnerabilities are found on that server.
* [Daniel Micay is stepping down as GrapheneOS Foundation director and as lead developer](https://twitter.com/DanielMicay/status/1662212227561308160)
