---
title: "Journaling in vim"
date: 2022-08-24T08:14:10+04:00
categories: [ 'vim']
tags: [ 'vim']
draft: False
---

I came across this interesting post called [journaling in vim](https://web.archive.org/web/20220511131921/https://danishpraka.sh/2020/02/23/journaling-in-vim.html) by [Danish Prakash](https://danishpraka.sh/) which uses built-in functionality in Vim to make a painless digital journal workflow. The write-up is clear and I've been able to set up my workflow.

Following is what I've learned during the process.

## Vim templates
Vim templates allow you to specify a template to be used for new files with a certain extension.

A template is a `.skeleton` file which is applied the Vim buffer.

See `:help template` for more.

### Skeleton file example
A simple example is shell script. Shell scripts starts with the shebang.

```
#! /usr/bin/env bash
```

Create your skeleton file in in `~/.vim/skeleton.sh` or `~/.vim/templates/skeleton.sh` and add the shebang header.
You can add other information such as usage or license to the template.

## Preloading the buffer

Autocommands are used to popuplate the buffer from the skeleton file when the buffer metadata matches a certain pattern.

```
autocmd VimEnter */journal/**   0r ~/.vim/templates/journal.skeleton
```
Let's break it down:

* `autocmd` -  command to be executed automatically when read or writing a file, when entering or leaving a buffer or window and when exiting Vim.
* `VimEnter` -  everytime vim is entered from the command line, the `autocmd` is trigger.
* `*/journal/**` -  the `autocmd` is triggered on the pattern `*/journal/**`
* `0r` -  read into the buffer starting at line 0.
* `~/.vim/templates/journal.skeleton` -  the file to read into the new file


### Autocommands structure
```
autocmd VimEnter */journal/**   0r ~/.vim/templates/journal.skeleton
        ^        ^              ^
        |        |              |
        |        |              The command to run. 
        |        |
        |        A "pattern" to filter the event.
        |
	The "event" to watch for.
```

### Augroup

Autocommands can be put together. This is useful for removing or executing a group of autocommands.


```
augroup journal
    autocmd!

    " populate journal template
    autocmd VimEnter */journal/**   0r ~/.vim/templates/journal.skeleton
augroup end
```
* `autocmd!` - Remove ALL autocommands for the current group

## Completion

Vim's completion feature allows us to select custom sources.
Setting a custom completion source:
```


autocmd VimEnter */journal/**   setlocal complete=k/Users/danish/journal/**/*
```
Break down:
* `setlocal` - set only the value local to the current buffer or window.
* `complete=k` - scan the files given the dictionary option.
* `**` - wildcards stands for a recursive descent into subdirectories

If you want to keep other default locations, use `:set complete+=` to add to it.

See `:help cpt` for more.

## Vimscript

Learning [vimscript the hard way](https://learnvimscriptthehardway.stevelosh.com/) is in my to-do list. 

---

## Notes

### Populating files which does not match pattern

Vim can read skeleton into the buffer. 
```
:read ~/.vim/templates/skeleton.sh
```

### Use BufNewFile instead of VimEnter

If you re-open a file which has been populate by the `augroup journal`, there will be duplication of the skeleton. To avoid this, you can use `BufNewFile`.
* `BufNewFile` - When starting to edit a file that doesn't exist.

```
augroup journal
    autocmd!

    " populate journal template
    autocmd BufNewFile */journal/**   0r ~/.vim/templates/journal.skeleton
augroup end
```




## Resources:
https://levelup.gitconnected.com/reducing-boilerplate-with-vim-templates-83831f8ced12


https://shapeshed.com/vim-templates/
