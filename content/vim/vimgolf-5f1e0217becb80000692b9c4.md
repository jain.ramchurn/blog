---
title: "Vimgolf 5f1e0217becb80000692b9c4"
date: 2022-08-01T15:36:23+04:00
draft: False
categories: [ 'vim', 'vimgolf']
tags: ['vim', 'vimgolf']
---

Link: http://www.vimgolf.com/challenges/5f1e0217becb80000692b9c4

# Rural post

Simple challenge to remove all but the post code on each line

### Start file

```
RD 5 Gore 9775
RD 6 Gore 9776
RD 7 Gore 9777
RD 1 Great Barrier Island 0991
RD 1 Greta Valley 7387
RD 1 Greytown 5794
RD 1 Hamilton 3281
RD 2 Hamilton 3282
```

### End file

```
9775
9776
9777
0991
7387
5794
3281
3282
```

## Solution:

```
.,$norm $bd0j
```

### Explanation:
```
.,$  - every line from current one till end of  file
norm - normal mode
$    - go to end of line
b    - go to start of word
d0   - delete  till start of line
j    - move one line down
```

### RECAP

#### Ranges:
```

Here are the most interesting ranges you can use:

*    <number> - Any number <number> in your range refers to a line number.
*    . - Represent the current line (often the default range).
*    $ - Represent the last line of the current buffer.
*    % - Represent the entire file (same as 1,$).
*    * - Use the last selection you’ve made during the last VISUAL mode.

For example, using the command :d:

*    :1,40d - Delete line 1 to 40 included.
*    :2,$d - Delete every line from the second one till the end of the file.
*    :.,$d - Delete every line from the current one till the end of the file.
*    :%d - Delete every line.
```

#### :norm[al]

```
:norm[al][!] {commands}                 *:norm* *:normal*
                        Execute Normal mode commands {commands}.  This makes
                        it possible to execute Normal mode commands typed on
                        the command-line.  {commands} are executed like they
                        are typed.  For undo all commands are undone together.
                        Execution stops when an error is encountered.

```


