---
title: "Vimgolf 5ba020f91abf2d000951055c"
date: 2022-07-31T20:46:27+04:00
draft: False
categories: [ 'vim', 'vimgolf']
tags: [ 'vim', 'vimgolf']
---

http://www.vimgolf.com/challenges/5ba020f91abf2d000951055c

# Com(m)a Trouble

Someone was real stupid when placing his commas. Can you fix it?

### Start file

```
,0,1,2,3,4,5,6,7,89
,1,2,3,4,5,6,7,8,90
,2,3,4,5,6,7,8,9,01
,3,4,5,6,7,8,9,0,12
,4,5,6,7,8,9,0,1,23
56,7,8,9,0,1,2,3,4,
67,8,9,0,1,2,3,4,5,
78,9,0,1,2,3,4,5,6,
89,0,1,2,3,4,5,6,7,
90,1,2,3,4,5,6,7,8,
```

### End file

```
0,1,2,3,4,5,6,7,8,9
1,2,3,4,5,6,7,8,9,0
2,3,4,5,6,7,8,9,0,1
3,4,5,6,7,8,9,0,1,2
4,5,6,7,8,9,0,1,2,3
5,6,7,8,9,0,1,2,3,4
6,7,8,9,0,1,2,3,4,5
7,8,9,0,1,2,3,4,5,6
8,9,0,1,2,3,4,5,6,7
9,0,1,2,3,4,5,6,7,8
```

## Solution:

```
<C-V>Md6E.0p{EPZZ
```

### Explanation:

```
<C-V> - visual mode
M     - Go to the first non-blank characterwise
d     - delete selected text
6E    - Move to the end of the 6th line below
.     - Delete character
0     - Move to the start of line
p     - Paste the delete character (,)
{     - Move to the start of paragraph
E     - Move to end of line
P     - Paste the text for the cursor
ZZ    - Save and quit
```
