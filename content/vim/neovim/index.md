---
title: "Switching to Neovim"
date: 2022-12-31T14:41:08+04:00
categories: [ 'neovim' ]
tags: [ 'vim', 'neovim', 'lunarvim', 'text editor' ]
draft: false
---

I've been using Vim for almost 4 years now - I've began my journey by reading [Mastering Vim Quickly](https://jovicailic.org/mastering-vim-quickly/), which I highly recommend, to upskill and become more confident in using Vim. The book is a fine balance between explanations, examples and tips. 

I've tried sublime, vscode and [vscodium](https://github.com/VSCodium/vscodium)(vscode without Miscosoft telemetry) with the Vim extension which were, however, clunky to use. Though I liked the vscode/vscodium interface, I missed Vim's keyboard and text centrism, and editing effiency. So I stayed with Vim and explored more of its plugin system.

I wanted to configure Vim as a light-weight IDE with syntax highlighting, auto-completion, file explorer, pair completion, colorschemes and so on. Some features were easy to implement while others would break and not work.

One of the features I found the most essential was auto-completion and I could not make it work in Vim. I wanted to have auto-completion for Terraform precisely, and I stumbled upon this blog post [My Neovim workflow for terraform](https://web.archive.org/web/20220526035639/https://mukeshsharma.dev/2022/02/08/neovim-workflow-for-terraform.html) which seemed exactly what I wanted. I followed the post and made it worked. Nevertheless, my happiness was short-lived because as soon I ended my session and open Neovim again, there was many errors output and the auto-completion were not working anymore.

## Neovim from stratch
So I was determined to learn Neovim from stratch and I've found a very good [tutorial](https://github.com/LunarVim/Neovim-from-scratch) which is accompanied by a playlist.

## Neovim: best text editor
Neovim takes it to another level.
> Neovim = extensibility + usability

Neovim is engineered for extensibility and usability and support:
- language server protocol (LSP)
- terminal emulator
- remote plugins
- vimscript parser
- treesitter integration
- [XDG Base Directory Specification](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html)

Here's the [roadmap](https://neovim.io/roadmap/) for Neovim.

Going down the rabbit hole, I came across  [astrovim](https://github.com/AstroNvim/AstroNvim) and [lunarvim](https://lunarvim.org) via [neovimcraft](https://neovimcraft.com/). I preferred to try lunarvim as its tutorial were already familiar.

## LunarVim: a light-weight IDE
Lunarvim is an IDE layer for Neovim and supports:
 - integrated terminal
 - file tree explorer
 - fuzzy searching
 - LSP
 - dashboard
 - multiple color themes

![neovim showcase](./img/neovim-showcase.png)

### Configuration

Lvim comes with its own config in its own location. Hence you can have nvim and lvim simultaenously. The configuration can be access from the welcome screen (pressing c) or `<leader>Lc` anytime.

### Integrated Terminal
The terminal can be access in floating mode via keybinding `ctrl+\`.
Furthermore, terminal can be set on the horizontal `alt+1`, vertical `alt+2` and floating `alt+3`.
Also, tmux or byobu can be run from the integrated terminal.

### keybinding

Lunar's keybindings can be easily viewed by `<leader>Lk` or command `:map`.

### colorschemes

Color schemes can be changed on the fly by command `:Telescope colorscheme` or `<leader>sp`.

I will write another blog post on LunarVim with more details, tips, and tricks once I learn all the features and plugins.


## Vim
Vim will remain, nonetheless, my default text editor on remote access.





