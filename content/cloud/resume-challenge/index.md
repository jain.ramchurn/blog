---
title: "Cloud Resume Challenge"
date: 2023-06-14T17:54:13+04:00
categories: [ 'cloud' ]
tags: [ 'AWS' ]
draft: false
---

The [cloud resume challenge](https://cloudresumechallenge.dev) is a self-directed learning project created by Forrest Brazeal, a cloud architech and AWS Community Hero. It is designed to help individuals enhance their cloud computing skills and create a professional-looking resume website using various cloud technologies.


### The Challenge and Its Structure

The challenge revolves around building a serverless web application that serves as a personal resume or portfolio website. It provides a structured format, guiding participants through a series of tasks and milestones to complete the project successfully.


### My Experience with the Cloud Resume Challenge on AWS

I recently attempted the Cloud Resume Challenge on AWS and documented my journey, including the project's architecture, design decisions, and the difficulties I encountered. Let's take a closer look at the tasks I completed:

1. Earn an AWS Certification

The challenge recommends obtaining the AWS Cloud Practitioner certification for the AWS edition. Although I haven't taken the exam yet, I plan to do so soon. However, I chose to prioritize the AWS Certified Solutions Architect - Associate (SAA-C03) certification based on Adrian Cantrill's insightful [video](https://www.youtube.com/watch?v=WK4SwQaPUXM) explanation.

2. Converting my resume in HTML

The challenge requires the resume to be written in HTML. Initially, [my resume](https://zain.mu/jain-ramchurn-curriculum-vitae.pdf) was in pdf format. I've used the [tool pdf2htmlEX](https://github.com/coolwanglu/pdf2htmlEX) to convert the pdf file into html.

Here's the command I used:

```
pdf2htmlEX --zoom 2.5 resume.cv resume.html
```

Additionally, I searched online for an appropriate template for the error page, which I plan to implement soon with some assistance.

3. Deploy the resume to Static Website with AWS S3.

To host my resume website, I created an S3 bucket with the same URL as my website. I prioritized security by blocking public access and implemented Origin Access Control (OAC) to restrict access to the CloudFront distribution. After setting up the CloudFront distribution, I applied the appropriate policy to the S3 bucket to allow the CloudFront Distribution access.


4. Enabling HTTPS Protocol with AWS CloudFront

During the setup of the CloudFront distribution, I encountered an issue related to service limits.

![cloudfront-issue](cloudfront-limit.png)

After a little DuckDuckgo, I found the solution [here](https://repost.aws/questions/QULXHEAzC7Sai6_LTLNYn83Q/questions/QULXHEAzC7Sai6_LTLNYn83Q/your-account-must-be-verified-before-you-can-add-new-cloudfront-resources?). I created a ticket to increase the service limit for CloudFront Distribution, within a day, my account was verified, and I could proceed with launching the CloudFront Distribution.

To ensure secure communication, I configured HTTP requests to redirect to HTTPS and enabled SSL certificate validation for my subdomain.

5. Pointing a Custom DNS Domain Name with AWS Route 53

Since I already had my custom domain, zain.mu, I created a subdomain, resume.zain.mu, in my domain registrar. Subsequently, I created a hosted zone for it in Route 53. To establish the connection, I added the Amazon NS for my subdomain in the domain registrar and created an alias to the CloudFront Distribution in Route 53. Moreover, I added a CNAME record in my domain registrar to verify the SSL certificate issued.

```
# dig +trace resume.zain.mu
;; global options: +cmd
...
zain.mu.		43200	IN	NS	ns-109-c.gandi.net.
zain.mu.		43200	IN	NS	ns-138-b.gandi.net.
zain.mu.		43200	IN	NS	ns-193-a.gandi.net.
;; Received 121 bytes from 193.0.9.98#53(udns2.tld.mu) in 313 ms

resume.zain.mu.		10800	IN	NS	ns-1254.awsdns-28.org.
resume.zain.mu.		10800	IN	NS	ns-526.awsdns-01.net.
resume.zain.mu.		10800	IN	NS	ns-284.awsdns-35.com.
resume.zain.mu.		10800	IN	NS	ns-2019.awsdns-60.co.uk.
;; Received 183 bytes from 217.70.187.110#53(ns-109-c.gandi.net) in 43 ms

resume.zain.mu.		60	IN	A	52.85.254.26
resume.zain.mu.		60	IN	A	52.85.254.81
resume.zain.mu.		60	IN	A	52.85.254.27
resume.zain.mu.		60	IN	A	52.85.254.61
resume.zain.mu.		172800	IN	NS	ns-1254.awsdns-28.org.
resume.zain.mu.		172800	IN	NS	ns-2019.awsdns-60.co.uk.
resume.zain.mu.		172800	IN	NS	ns-284.awsdns-35.com.
resume.zain.mu.		172800	IN	NS	ns-526.awsdns-01.net.
```

6. Implementing a Webpage Visitor Counter with JavaScript

As I'm not an experienced programmer, I relied on ChatGPT to help me write the JavaScript code for the webpage visitor counter. After a few attempts, I successfully utilized the Fetch API to send a POST request to the URL associated with a Lambda function. This request included the necessary data to increment the visitor count in the database. The Lambda function processed the request, retrieved the current count from the database, incremented it by one, and updated the value in the database.

Once the Lambda function processed the request and updated the count, it returned a JSON response to the Fetch API call. I used JavaScript to extract the count from the JSON response and displayed it at the bottom of the webpage.


7. Creating a Visitor Counter Database with AWS DynamoDB

Setting up the visitor counter database in DynamoDB was a straightforward process. I created a single table with a single attribute to hold the visitor count. To update the count in the DynamoDB table, I utilized the previously mentioned Lambda function. This Lambda function, triggered by the Fetch API POST request, retrieved the current count from the table, incremented it by one, and persisted the updated value back to the table.

8. Connecting the Webpage to the Database with AWS API Gateway + Lambda

Although I skipped the implementation of API Gateway due to my limited understanding, I plan to explore it further in my next iteration of the challenge. This step would establish a connection between the webpage and the DynamoDB database.

9. Writing a Lambda Function with Python + AWS Boto3 

Within the Lambda function, I imported the Boto3 library and instantiated a DynamoDB client. This client served as the interface to interact with DynamoDB. Using the client, I retrieved the current count value from the DynamoDB table by specifying the table name and the desired key.

After obtaining the current count, I incremented it by one to reflect the new visitor. The incremented value was then persisted back to the DynamoDB table, ensuring that the count remained accurate and up-to-date for future requests.

### Conclusion

I successfully completed the Cloud Resume Challenge using the AWS console, and while I skipped the implementation of Infrastructure as Code (IaC), version control, and CI/CD, I plan to revisit the challenge using Terraform and AWS SAM in future iterations.

I am grateful to Forrest Brazeal for creating this challenge, which has not only helped me showcase my cloud computing skills but also enabled me to learn new technologies along the way.

You can explore my completed resume website hosted on AWS by visiting https://resume.zain.mu

Thank you for joining me on this journey of skill enhancement and technology exploration through the Cloud Resume Challenge.
