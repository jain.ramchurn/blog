---
title: "Browser Origins"
date: 2025-03-03T15:33:01+04:00
categories: [ 'tech' ]
tags: [ 'tech', 'history', 'browser']
draft: false
---

Last Saturday, I attended the first [Frontend.mu meetup](https://frontend.mu/meetup/61) of 2025. One of
the featured presentations was on "Browser Origins," delivered by [Sandeep](https://sandeep.ramgolam.com/).
The talk was very interesting, and I'd like to share what I learned from it.
You can find Sandeep's presentation [here](https://www.figma.com/proto/pnQG2FXyqzXyxIn8Uwn2Zi/Meetup-March-2025?node-id=3-1343&p=f&t=PHHv4CLC643nX3Wr-1&scaling=min-zoom&content-scaling=fixed&page-id=0%3A1&starting-point-node-id=3%3A1343&show-proto-sidebar=1).

## The Early Days

The first web browser was created by Sir Tim Berners-Lee in 1990,
called "WorldWideWeb" as part of his effort to develop the World Wide Web.
It was later renamed *Nexus* to avoid confusion with the broader concept of the
web. Nexus allowed users to view and edit web pages,serving as both a browser an
d an editor. It was developer for the NeXT computer system and laid the
groundwork for all future browser.

Furthermore, Tim Berners-Lee founded the World Wide Web Consortium (W3C) in 1994
to promote Web standards to ensure interoperability across different browser and
platform.

## The Rise of Netscape and First Browser War

In 1994, Netscape Navigator emerged as the second major browser, created by Marc
Andreesen. Netscape introduced many features that are now standard, such as
support for images, JavaScript, and cookies. However, its dominance was
challenged by Microsoft's Internet Explorer (IE), which was bundled with the
Windows Operating system, giving it an unfair advantage over Netscape.
This resulted in a sharp decline in Netscape’s market share, and by the early
2000s, Internet Explorer dominated the web with over 90% market share. This
period was characterized by rapid innovation but also by the introduction of
proprietary features that fragmented web standards.

Also this had led to the famous Microsoft Corperation's Antitrust Case where
one of the allegations is that Microsoft illegally tried to monopolized
Internet browsers market.

## JavaScript vs JScript

JavaScript, created by Brendan Eich in 1995, was initially developed for
Netscape Navigator to add interactivity to web pages. It was originally
called *LiveScript* before being renamed *JavaScript* as a marketing
strategy to capitalize on Java's popularity. Microsoft later created its
own version called JScript for Internet Explorer, which led to
compatibility issues between browsers due to differences in
implementation, leading to widespread use of "best viewed in Netscape"
and "best viewed in Internet Explorer" logos for several years.

To standardize the language, ECMAScript was introduced as a specification
by Ecma International in 1997. ECMAScript serves as the foundation for
JavaScript, ensuring consistency across browsers and enabling new features
like arrow functions, async/await, and modules.

## Firefox and the Open Web

Mozilla Firefox was released in 2004 as a free and open-source browser, built
from the remnants of Netscape's codebase. Firefox introduced groundbreaking
innovations like tabbed browsing, a robust extension ecosystem, and a strong
focus on user privacy and web standards. These features, combined with its
extensibility through add-ons, helped Firefox gradually gain market share and
challenge IE's monopoly.

## Google Chrome and the Second Browser War

In 2008, Google Chrome was released, featuring a minimalist design, a powerful
JavaScript engine called V8, and a multi-process architecture that improved
performance and security. Chrome’s speed and simplicity led to rapid adoption,
igniting the second browser war. Competing browsers like Firefox, Safari, and
Edge struggled to keep up, and by the mid-2010s, Chrome became the dominant
browser worldwide.

## Web Engines: The Technical Core

At the core of every modern browser is a web engine, responsible for parsing
and rendering web content. The major engines include:

* Blink: Developed by Google (forked from WebKit), powers Chrome, Edge, Opera,
and many others. It has become the most widely used engine.
* Gecko: Mozilla's engine for Firefox, focuses on standards compliance and
performance.
* WebKit: Originally developed by Apple, powers Safari and was the foundation
for Blink.
* Trident/EdgeHTML: Microsoft's legacy engines (now discontinued in favor of
Blink).

These engines differ in how they handle rendering, JavaScript execution, and
support for emerging web standards. For example, Blink and WebKit share a
common ancestry but have diverged in their implementation of features like CSS
grid and JavaScript APIs. Gecko, on the other hand, has a distinct architecture
that prioritizes extensibility and adherence to open standards.

## Baseline and Interop: Measuring Web Compatibility

The [Web Platform Status project](<https://webstatus.dev/>) and [Interop](<https://wpt.fyi/>) are initiatives aimed
at improving cross-browser compatibility and ensuring consistent implementation
of web standards. **Baseline** is a concept introduced to define a set of web
features that are universally supported across all major browsers.
This helps developers build websites that work reliably without needing
polyfills or workarounds.

Interop, on the other hand, focuses on identifying and resolving inconsistencies
in how browsers implement web standards. By running a vast suite of tests
through the Web Platform Tests (WPT), Interop provides a detailed breakdown of
browser compatibility, encouraging vendors to address gaps and improve
interoperability.

## The Future of Browsers

The browser landscape continues to evolve, with new players challenging the
status quo.

* Arc Browser is a modern browser designed for productivity, featuring a unique sidebar interface and advanced organizational tools.
* Ladybird, a browser being developed by the SerenityOS project, aims to create a fully independent engine from scratch, free from the constraints of existing engines.
* Zen Browser focuses on minimalism and privacy, offering a distraction-free browsing experience.
