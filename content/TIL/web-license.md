---
title: "Web License"
date: 2022-08-24T17:48:46+04:00
draft: False
tags: [ 'TIL']
categories: [ 'TIL']
---

TIL CERN developed its own open-source [licence](https://web.archive.org/web/20220120231957/https://home.cern/science/computing/birth-web/licensing-web) and the Web version 3.0 was the first and also last one release by CERN of Open Source software before W3C at MIT took over and then release WWW(HTTPD) version 3.1 under the MIT licence.

