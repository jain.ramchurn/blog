---
title: "People Kernel Org"
date: 2022-08-24T16:30:55+04:00
categories: [ 'TIL']
draft: False
---

TIL about [people.kernel.org](https://people.kernel.org/read) which is a collection of individual blogs authored by Linux kernel developers. It is powered by WriteFreely and federated via ActivityPub.

For now, the service is available to high-profile developers and maintainers of the Linux Kernel.


