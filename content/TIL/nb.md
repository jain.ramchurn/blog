---
title: "nb"
date: 2022-10-13T11:01:14+04:00
categories: [ 'TIL' ]
tags: [ 'TIL', 'CLI' ]
draft: false
---

TIL about [`nb`](https://github.com/xwmx/nb) which is a command line and local web note-taking, bookmarking, archiving, knowledge base application. It is written in `bash` and even has a [`Go`](https://github.com/xwmx/nb/tree/master/nb.go) implementation.


