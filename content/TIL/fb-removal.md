---
title: "Facebook's new tool"
date: 2022-11-07T19:23:52+04:00
categories: [ 'TIL' ]
tags: [ 'privacy','TIL', 'Facebook' ]
draft: false
---


TIL about Facebook (or Meta)'s new [tool](https://www.facebook.com/contacts/removal) allowing people to check if the former has their contact information such as phone number and email address, even if they have never handed them over, and delete the data.

Facebook [collects data about non-users](https://www.facebook.com/help/637205020878504) via a user's address book when permission is granted. Data collected are: names, phone numbers and email addresses. These information are processed to:
 * check if the numbers or email addresses belong to users.
 * suggest the contacts as [People You May Know](https://www.facebook.com/help/1163341187771886) on Facebook and [recommend them as accounts to follow on Instagram](https://help.instagram.com/1128997980474717).
 * invite non-users to join the Meta platform.
 * investigate suspicious activities and keep the platform safe and secure.

 When the contact information is requested to be deleted, a copy is kept in a block list to prevent it from being uploaded again through someone's address book.


