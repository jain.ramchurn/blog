---
title: "Terraform Theory"
date: 2022-08-08T15:45:30+04:00
tags: [ 'terraform', 'devops']
categories: [ 'devops']
draft: false
---

Terraform is an open-source infrastructure as code software tool that  provides a consistent CLI workflow to manage hundreds of cloud services. Terraform codifies cloud APIs into declarative configuration files.

Terraform helps you manage all of your infrastructures as code and  construct it as and when needed. Here are its key main features:

- A console that allows users to observe functions
- The ability to translate HCL code into JSON format
- A configuration language that supports interpolation
- A module count that keeps track of the number of modules applied to the infrastructure.

Terraform commands:

- **terraform init** - initializes the current directory
- **terraform refresh** - refreshes the state file
- **terraform output** - views Terraform outputs
- **terraform apply** - applies the Terraform code and builds stuff
- **terraform destroy** - destroys what has been built by Terraform
- **terraform graph** - creates a DOT-formatted graph
- **terraform plan** - a dry run to see what Terraform will do
- **terraform validate** - check whether the configuration is valid

Each Terraform configuration can specify a backend, which defines two main things:

- Where operations are performed
- Where the state is stored (Terraform keeps track of all the resources created in a state file)

## MODULES

*Modules* are containers for multiple resources that are used together. A module consists of a collection of `.tf` and/or `.tf.json` files kept together in a directory.

ROOT MODULES

Every Terraform configuration has at least one module, known as its *root module*, which consists of the resources defined in the `.tf` files in the main working directory.

CHILD MODULES

A module that has been called by another module is often referred to as a child module.

Automatically download and install community or partner modules from the registry with `terraform init`

## RESOURCES

Each resource block describes one or more infrastructure objects, such as virtual networks, compute instances, or higher-level components such as DNS records.

## PROVISIONERS

Provisioners documents configuring post-creation actions for a resource using the `provisioner` and `connection` blocks.

## STATE FILE LOCKING

State file locking is Terraform mechanism in which operations on a  specific state file are blocked to avoid conflicts between multiple  users performing the same process. When one user releases the lock, then only the other one can operate on that state. This helps in preventing  state file corruption. This is a backend operation.


## TAINTED RESOURCES
A tainted resource is a resource that is forced to be destroyed and  recreated on the next apply command. When a resource is marked as  tainted, the state files are updated, but nothing changes on  infrastructure. The terraform plan out shows that help will get  destroyed and recreated. The changes get implemented when the next apply happens.
