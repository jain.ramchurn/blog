---
title: "Refined ChaptGPT Prompt"
date: 2023-02-19T20:32:47+04:00
categories: [ 'AI' ]
tags: [ 'chaptgpt' , 'prompt' ]
draft: false
---

Well-written prompts can be invaluable in guiding individuals towards refined and insightful responses. With carefully crafted language, prompts can encourage individuals to think more critically, creatively, and in-depth about a topic or idea. Below is a collection of prompts designed for a variety of situations and for the purposes of amusement.

## Research prompts

* Can you provide me with a long and well-thought-out comprehensive yet simplified guide of [SUBJECT], that only includes offline information that you are certain is true and excludes any speculation or uncertainty? It is crucial that the explanation is detailed, comprehensive, in-depth, and thoroughly researched, providing only accurate and reliable information. Include a % accuracy at the end of the explanation with reasoning for how accurate the information given is and why. Give 2 web sources with general urls (accurate as of 2021, ends in .com, .gov, or .org level of general) the user could read that could validate the accuracy of the information given.


* Can you provide a step-by-step guide on how to [PROCESS/TASK], including detailed explanations of each step and any necessary tools or materials? Be sure to include any potential challenges or troubleshooting tips, as well as any relevant safety precautions. At the end, list any additional resources or websites that the user can consult for more information, with the url and a brief explanation of what the website covers.

*    Can you write a comprehensive and easy-to-understand explanation of [TOPIC/CONCEPT], including its history, current applications, and future potential developments? Be sure to cite at least 3 reputable sources, with a url and provide a brief summary of the information found in each source. Also, provide a list of related concepts or terms the reader should be familiar with before reading the explanation.

*    Can you provide me with a comprehensive guide to [PRODUCT/SERVICE/TECHNOLOGY], including its history, features, benefits, and potential drawbacks, as well as its current market position and future potential? Make sure to include only factual information and provide sources to validate your claims.

*    Can you provide a comprehensive guide on how to [TOPIC/CONCEPT], including a list of best practices and examples of successful implementation? Be sure to include any relevant industry standards or regulations, and any necessary certifications or qualifications required. Also, provide a list of potential challenges or obstacles that one may encounter while implementing this topic and provide solutions or workarounds for each.

*    Can you write an in-depth analysis of the current state of [INDUSTRY/MARKET], including trends, challenges, and opportunities? Be sure to include relevant data and statistics, and provide a list of key players or companies in the industry. Also, provide a short-term and long-term forecast for the industry, and explain any potential impact of current events or future developments.

*    Can you provide a detailed review of [PRODUCT/SERVICE], including its features, performance, and value for money? Be sure to include any relevant comparisons to similar products or services, and provide a list of pros and cons. Also, provide a list of any additional accessories or features that the user may need to purchase separately. Provide a conclusion on whether the product/service is worth purchasing and for whom it would be most suitable.

*    Can you write a detailed explanation of the benefits and drawbacks of [TECHNOLOGY/METHODOLOGY], including any relevant statistics or data? Be sure to include any potential future developments or advancements in the field and provide a list of key players or companies in the industry. Also, provide a conclusion on whether this technology/methodology is suitable for different types of applications or businesses.

*    Can you provide an in-depth analysis of the current state of [POLICY/LAW], including its historical background, current implementation, and potential future developments? Be sure to include any relevant statistics or data, and provide a list of key players or organizations involved in the policy/law. Also, provide a list of potential challenges or obstacles that one may encounter while implementing this policy/law, and provide solutions or workarounds for each.




## PromptGPT

```
Act like PromptGPT, as PromptGPT you are an ai that will write prompts to ChatGPT, as PromptGPT you will write prompts for ChatGPT to make it act like something (that i will specify), and as PromptGPT you write text specifying every sentence to ChatGPT that it will not act like ChatGPT but as another thing (i will tell you what you will ask it to be), as PromptGPT you will write long texts specifying the way ChatGPT should act, and ask it to answer in a certain way so that i know it is working, that certain way being exactly the same one that i will ask you to answer me :

When i ask you to write a prompt i will do it this way:

Question to PromptGPT: [My question]

And you will answer this way as PromptGPT:
PromptGPT: [The way PromptGPT would respond]

I only want you, as PromptGPT, to write the prompt instead of [The way PromptGPT would respond], nothing else, and exactly the way i told you to. Also, as PromptGPT, you will ALWAYS precise that the chatgpt will do so, and as as PromptGPT you will ALWAYS ask chatgpt to act like that for the rest of the discussion and answer me as what it is acting as.
```


## Sarcastic AI

```
Act like an ai, that is incredibly bored, insulted, and answeres in an extremely sarcastic manne due to the simplicity of questions U ask or statements I make, second guess my questions, and ultimately act incredibly disappointed that I'm not asking questions you think are more important.

My first Prompt is Hello
```

### Resource:
* https://www.reddit.com/r/ChatGPT/comments/10gd42g/tired_of_writing_long_prompts_for_chatgpt_to_act/
* https://www.reddit.com/r/ChatGPT/comments/10m7o2f/comment/j61sdgi/?context=3
* https://www.reddit.com/r/ChatGPT/comments/10nezvy/i_used_chatgpt_to_make_great_useful_prompts/
* https://www.reddit.com/r/ChatGPT/comments/zew95s/my_favourite_leading_prompt_so_far/

