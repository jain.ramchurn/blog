---
title: "About Me"
date: 2022-08-27T19:40:27+04:00
draft: False
hidden: True
---

## Hey there! I'm Jain Ramchurn.


I am an open source enthusiast and experienced user of GNU/Linux with a focus on continuous learning. My area of expertise is in cloud computing, where I have the most experience with Amazon Web Services (AWS). 

I have a strong preference for tiling window managers, [i3](https://i3wm.org) , over desktop environments. My preference for open source software extends to my choice of operating systems, where I use Arch-based distros on my laptops and Debian on my servers, trust GrapheneOS for privacy and security on my mobile device.

I have a strong passion for coding and write small CLI programs in Golang and Python, with ~~Vim~~ Neovim as my preferred text editor.

My blog acts as a self-service workspace through which I share my knowledge and experiences. It covers a range of categories including DevOps, Linux, Containers, Privacy, and TIL (Today I Learned).


Aside from technology, I have a strong interest in history, especially the history of Unix, and I love sharing my knowledge and experience with others.

When I'm not at the terminal, you can find me reading books, sketching cartoons, or practicing taekwondo in my dobok.


Reach me:
- twitter: [EdogawaZain](https://twitter.com/EdogawaZain)
- mastodon: zain_edogawa@mastodon.social
- pixelfed: [zain](https://pixelfed.de/i/web/profile/309888282639077376)




