---
title: "Unix CLI Conventions Over time"
date: 2022-08-10T21:26:43+04:00
tags: ['unix', 'cli']
categories: ['unix']
draft: False
---

Awesome [blog post](https://web.archive.org/web/20220601175501/https://blog.liw.fi/posts/2022/05/07/unix-cli/) explaining how the syntax of Unix command line has varied over the years, and in different subcultures.


