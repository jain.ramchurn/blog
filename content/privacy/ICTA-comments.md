---
title: "Comments on the Consultation Paper on proposed amendments to the ICT Act for regulating the use and addressing the abuse and misuse of  Social Media In Mauritius"
date: 2022-11-11T20:22:07+04:00
categories: [ 'privacy' ]
tags: [ 'privacy' ]
draft: false
---

[On 14 April 2021](https://www.icta.mu/comm-social-media-consult/), the Information and Communication Technologies Authority launched a Public Consultation on the [proposed amendments](https://www.icta.mu/documents/2021/10/Social_Media_Public_Consultation.pdf) to the ICT Act for regulating the use and addressing the abuse and misuse of Social Media in Mauritius.  The plan was to set up a National Digital Ethics Commitee with an enforcement unit empowered to take down and censor social media posts and a local proxy server to act as an intermediary which will segregate traffic to and from social media sites, decrypt information and, analyse and store data for investigations purposes.


Below are my comments submitted to ICTA.

---


20 May 2021

## Introduction

The ICTA has made a proposal for amendments that aim to put in place a tool between social media and Mauritian users so as to be able to block and filter contents that will be deemed harmful. 

The purpose of this tool will be to decrypt normally encrypted data with the aim of analyzing and "archiving" it in order to eventually be able to trace dangerous content and its authors.

##  Comments on  the lack of definition of social media and lists of the platforms involved

The ICTA  has not clearly defined social media to remove ambiguity and does not provide a  list of  the  social media concerned.   The ICTA  set Facebook  as an example of social media in its consultation paper.  Social  media  are  not  limited  to  social  networks  like  Facebook  but  include  blogs,  business  networks,  collaborative  projects,  enterprise  social  networks,  forums,  microblogs,  photo  sharing,  product/services  reviews,  social  bookmarking,  social  gaming,  video  sharing  and  virtual  worlds. 

* Are we including all the 13 types of social media?
* Is there an exhaustive list of the social media concerned? 
* Are only popular social media such as Facebook, Instagram, Tiktok, Pinterest, Snapchat, Youtube and Twitter concerned?
* Are lesser known  social media in Mauritius such as Reddit, 9gag,  Mastodon, Friendica, GNU social, Peertube, Plume, Odysee, the Fediverse among others, also  involved?
* Are social media such as 4chan, 8chan, 9chan, Ylilauta, FapChan, Wizardchan, among others which maintains the anonymity of the users by  not having a registration system and allowing users to post anonymously,  also affected by the proposal?

It is imperative that the ICTA provides answers  to these types of social media targeted so as to enhance transparency. 

Furthermore, the term "harmful" is not clearly defined. This leads to speculation to what content is deemed to be harmful and what criteria is used to classify content to be harmful. Undoubtedly, this leaves a  a cloud of subjectivity in the process.

## Comments on Section 4: Measures taken at the international level

While it is good to look up for countries which have faced similar problems and study their solutions and methods of implementation, we should also look on the other side of the coin, that is, the repercussion and critics of the measures taken, before taking a well informed decision.

### Germany

Gemany's Network Enforcement Act, or NEtzDG for short, has been heavily criticized in Germany and abroads, and experts have been suggested that it interferes[^1] with the EU's central  Internet regulation, the e-Commerce Directive. Critics have also pointed out that the strict time window to remove content does not allow for a balance legal analysis [^2]- companies does not have sufficient time to evaluate content before implementing penalties.

### France

A large sheer number of organizations and individuals have criticized the "Avia law, Loi No. 2020-766 du 24 juin 2020 " as a danger to freedom of expression, in particular because of the possibility that decisions to remove content could be taken by a private operator without the intervention of the judicial judge, who is the constitutional guarantor of individual freedoms (Article 66 of the Constitution). Some of the organizations are:

* professional organizations: Association des services Internet communautaires (ASIC), Syntec Numérique et Tech In France[^3]

* organizations and associations:  AIDES[^4], Association Aeon[^5] , Association des Avocats Conseils d’Entreprises[^6], Autres regards[^4], Bus des femmes[^4], Cabiria[^4], Change.org[^5], Commission nationale consultative des droits de l'homme[^6], Conseil national des barreaux,[^7], Conseil national du numérique[^8], European Digital Rights (EDRi)[^9], European Internet Services Providers Association (EuroISPA)[^9], Internet sans frontières[^7],  Syndicat des avocats de France[^6]

* idea laboratories:  Génération libre[^10]

* the European commission[^11]

### Australia

The Criminal Code Amendment (Sharing of Abhorrent Violent Material) Act 2019 ("the AVM Act") which was passed without any public or expert consultation [^12], creates incentives for tech companies to over-censor  in order to avoid the the threat of liability while failing to address the many deficits that currently exist in  online content moderation and associated regulation. [^13]

### India

Posts with hashtag #ResignModi  were temporarily  hidden by "mistake" on Facebook for around 3 hours[^14].  Facebook  reported that the block was the result of some of the content that used the hashtag but provided no specification for what kind of content were concerned.

The Indian government sent Twitter an emergency order to censor [52 tweets](https://cdn.vox-cdn.com/uploads/chorus_asset/file/22466918/April_23_2021_Lumen_India.pdf) which spoke about the mishandling of the Corona virus pandemic.

The Wall Street Journal has previously reported that India's government has threatened to jail employees of Facebook, its WhatsApp unit and Twitter in direct response to the companies’ reluctance to comply with data and take down requests [^15].

In February, Twitter blocked more than 500 accounts linked to the  ongoing farmer protests against agricultural reforms after the  government issued a legal notice[^16].

Social media companies run the risk of  being shut down entirely if the companies disobey the Indian government .

The above exemplify how India is stepping towards a kind of "digital authoritarianism" and how India  is on the verge of losing its status as  the greatest democracy to become an  autocracy. 

## Comments on the section 6: The local scene

The data provided by Mauritian Cybercrime Online System (MAUCORS) is poor and lacking.  The total number of incident reported is mentioned but no other details and information  are provided:

* What are the platforms involved and the number of cases for each?
* What is the  status ( investigating, pending or completed)  of the cases?
* What is the investigation procedures?
* What actions  were taken in those cases and what were the results?
* What is the incidents reported statistics that were dealt by local authorities and by the social platforms administrators?

<div style="page-break-after: always;"></div>
## Comments on the Toolset

### Proxy Server

The proxy server that will be setup, will monitor all the Internet traffic in Mauritius and will further decrypt and inspect the HTTPS traffic.

**The proxy server will impersonate the social media and intercept it to copy & archive all the sensitive information such as the login credentials.  Commercial payment transactions will also be decrypted and archived as the platforms are also used by small and medium enterprises.** Furthermore, websites that uses social media accounts as *identify authentication* will also be involved and thus be impacted. In other words, the  proxy server operated by the ICT Authority will be conducting  Man-In-The-Middle (MiTM) attacks.

**MiTM attacks  break confidentiality and integrity and  undermines security for all users.** Once a connection has been intercepted, severity can be increase  from simple spying to content injection.

Other concerns about the proxy server are as follows:

* What happens when the proxy server is down?

* What happens if the proxy server is hacked as archives are bound to invite hackers?

* What will be the duration of the data retention?

* Who will be responsible if there are data leaks?

* Will the data be subjected to Data Protection Laws of Mauritius even if it is store outside Mauritius?

* What is the guarantee  that no advertisement or tracking scripts will be injected?

* What is the  guarantee that the monitoring of the social media will not be expanded on all the Internet to spy on the citizen of Mauritius?

* What if there is misuse of information from within the organisation?

  
  
  

### Government-issue certificate

The introduction of the security certificate, ironically, is a direct attack on online privacy and free speech. There's a high chance that the government-issued certificate will be added to a block list as it will undermine security of the users.  Google, Mozilla and Apple has incorporated technical protection into their Chrome, Firefox and Safari browsers respectively to fight back middle-in-the-man attacks. When traffic-interception mechanism  are detected, the browsers block the connection and display a warning which cannot be bypass. 

The individual's security and privacy on the Internet are treated as fundamental and not as an optional.

Furthermore, there's also the risk of installing malicious root certificate from unofficial websites and resources which allows  attackers to decrypt the SSL traffic on the victim's machine.

It is also unclear what would happen to individuals who refuse to install the government-issue certificate, as the least we can assume is the encrypted services they use would become unavailable.

Please peruse  these resources below to see how Google and Mozilla has protected the security and privacy of theirs users:

* [protecting chrome users in kazakhstan](https://security.googleblog.com/2019/08/protecting-chrome-users-in-kazakhstan.html)

* [chromium source code](https://chromium.googlesource.com/chromium/src.git/+/f0596779e57f46fccb115a0fd65f0305894e3031)

* [Mozilla takes action to protect users in kazakhstan](https://chromium.googlesource.com/chromium/src.git/+/f0596779e57f46fccb115a0fd65f0305894e3031)

  


## Alternatives

### Education

[Educating](https://onlinecensorship.org/resources/further-readings) and sensitising people about the social media providers' products and their evolving policies will make the platform safer. Encourage people to read the  community standards  and other resources provided by the platform. For instance,  Facebook  has a [guide](https://about.fb.com/wp-content/uploads/2017/03/not-without-my-consent.pdf)  how to report and remove intimates images that are shared without consent.   Moreover, authorities can collaborate  with Facebook to produce safety resources  such as guides and videos  in different languages ( English, French and Creole) for Mauritius.  Below are four examples of safety resources provided by Kenya and  Pakistan:

* [Kenya's Think before you share](https://scontent.fmru4-1.fna.fbcdn.net/v/t39.2365-6/14658610_1073463726101691_2538660069499731968_n.pdf?_nc_cat=103&ccb=1-3&_nc_sid=ad8a9d&_nc_ohc=P_mDIV5ceWAAX-z3vfn&_nc_ht=scontent.fmru4-1.fna&oh=3940fbfe44d9d99b92cd84f00ea5cfbf&oe=60B51C13)
* Pakistan's  Privacy and Safety  on Facebook in [English](https://scontent.fmru4-1.fna.fbcdn.net/v/t39.2365-6/14662502_331483207202248_6865395761119494144_n.pdf?_nc_cat=102&ccb=1-3&_nc_sid=ad8a9d&_nc_ohc=8WmoimzcyREAX8TAyFz&_nc_ht=scontent.fmru4-1.fna&oh=3ead9d70cdc86ebd95fb88d632faaebc&oe=60B405D4)  and in [Urdu](https://scontent.fmru4-1.fna.fbcdn.net/v/t39.2365-6/14676833_512934092232876_3306558526482022400_n.pdf?_nc_cat=103&ccb=1-3&_nc_sid=ad8a9d&_nc_ohc=JQTjtL8R8UwAX8QRAnK&_nc_ht=scontent.fmru4-1.fna&oh=aa6881c8517117b65f96ade622cb4d64&oe=60B4911E)

Visit https://www.facebook.com/safety/resources for more information and ideas.

Inform people aware of the features developed for security and help to enhance them . For example, Instagram has  introduced a [feature](https://about.instagram.com/blog/announcements/introducing-new-tools-to-protect-our-community-from-abuse)  to filter abusive messages. Authorities can work with Instagram to develop a predefined list of offensive terms that can be filters from DM requests.

Making people to discern fake news helps to curtail harmful content. As we consume information and news more quickly, skimming Twitter, dipping into Instagram, so we lose track of what has substance and what does not. Teaching people to be sceptical , examining the sources and do  a little research and  fact-checking can  prevent harmful contents or misinformation to be posted. On Facebook,  stories that are flagged as false and are thus  shown lower in feeds. Twitter's prompt "Want to read the article first" opens when someone's share an article they have not open on Twitter. 

It is possible  that many  people has not access and read the [guideline on social networks]( http://cybersecurity.ncb.mu/English/Documents/Knowledge%20Bank/Guidelines/Guideline%20on%20Social%20Networks.pdf) by National Computer Board  Mauritius. Also, maybe it is time to update the guideline since it has last been edited in July 2011. 



 ###  Fact-checking organisation

Setting up an IFCN-certified organisation and becoming [Facebook's third-party fact-checking](https://www.facebook.com/journalismproject/programs/third-party-fact-checking)   helps manage and review content and if possible to review reports.  Contact  https://ifcncodeofprinciples.poynter.org/ for more information.

Additionally,  there are [training resources to aide in fact-checking]( https://www.facebook.com/journalismproject/programs/third-party-fact-checking/fact-checking-health-misinformation-resources).

Likewise, authorities can work closely with [Africa Check]( https://africacheck.org/), the first independent fact-checking organisation in Africa to debunk dangerous false statements .

###  Working in collaboration

If it is difficult to have regional offices in Mauritius, authorities must  at least work in collaboration with near headquarters and  offices such as Facebook's headquarter in  Johannesburg and the new office which will be open in [Lagos, Nigeria](https://about.fb.com/news/2020/09/opening-an-office-in-lagos-nigeria/) and become operational in 2021.

We must make use of our diplomatic connection at the regional level  to improve the  responsiveness to request from local authorities. 

### Laws

#### Gaming social media

Facebook became the platform of choice for political campaigners.  This includes  deploying memetic warfare tools,  amassing vast voter data sets, developing sophisticated behavioural targeting methods, or poisoning the community  with false information.  

Mauritius has sought the Britannic Cambridge Analytica firm[^17] to influence elections. 

Democracy in Mauritius was, plausibly,  hacked.

ICTA must prevent the use of  social media as  a propaganda machines by political parties.  Laws must be amended to forbid the use of ads or sponsored posts by political parties to prevent them to game social media, misuse and abuse social media  and hence limit their influence elections. 

#### Efficiency of Mauritian Law concerning cybercrimes.

The main specialized institutions in Mauritius against online crimes are:

- The Police force (IT Unit)
- The Information and Communication Technologies Authority ( ICTA)
- The Mauritian National Computer Security Incident Response Team (CERT-MU)
- The National Computer Board
- The Attorney General’s Office
- The Office of the Director of Public Prosecutions ( which also comprises of a cybercrime unit)
- The IT Security Unit

Though we have all these institutions, it still seems that Mauritian Law is not completely efficient against cybercrimes.  

The initial complaint can be lodged to an on-duty police officer. However, the issue should be redirected to a qualified and experience officer with an IT background in the view to  better monitor the particular matter. 

##### International cooperation 

According to the Mutual Assistance in Criminal and Related Matters Act  2003, the Attorney General, designated as the “Central Authority” for  the purposes of a request from a foreign State or an international  criminal tribunal, or a request from Mauritius to a foreign State or an  international criminal tribunal, is the appropriate competent authority.

 ##### The composition of the NDEC

ICTA propose a **National Digital Ethics Committee (NDEC)** as the decision-making body on the contents and ‘the Chairperson and members of the NDEC be **independent, and persons of high calibre and good repute**.’

The definitive criteria which the general terms ‘high calibre and good repute’ leave too open.

* How will these persons be selected and elected?
* What criteria and what qualification and expertise must those persons hold?

To what extend will the committee be independent from political interference if any? 

### "Misuse and abuse of social media"

####  Take down, censorship and arrests.

Below is an extract of section 2.A of the [country 2020  reports on human right](https://www.state.gov/reports/2020-country-reports-on-human-rights-practices/mauritius/):

```"Freedom of Speech:  As of November 30, police had arrested seven persons for antigovernment comments and postings on social media. [...] On April 15 , police arrested Rachna Seenuth, a civil servant who  formerly worked as the president’s secretary, after she shared a meme  that included a photo of the prime minister. Police arrested Ravin Lochun on July 9 for a similar offense. [...]  On July  25 , police arrested Farihah Ruhomally after she criticized Member of  Parliament Tania Diolle on Facebook."```

```"Censorship or Content Restrictions: Opposition politicians reported that their social media accounts were routinely blocked and their  antigovernment postings removed."```



The first part mention people who were arrested for causing  “annoyance, humiliation, inconvenience, distress or anxiety" to a person ( government official in those cases) on social media. The second part is plain censorship.

According to an article in L' express, "this ICTA proposal was introduced after  the government faced a  series of social media-fuelled protests over its mishandling of  Covid-19, Wakashio and the Kistnen case. [^18]"

Could this mean, coupled with the ICTA's proposal that more people will be arrested for criticising the government,  debating the the government's competency or  for generating and sharing of memes of "high-profile" people as posts, comments and private messages would be archived and retrieved easier and people will be easily tracked?

#### Internet Societies Statements

Many Internet societies and international organisations have raised concerns and accessed the impact of the consultation paper by  ICTA. All of them, it is said the proposed actions threatens the privacy and freedom of expression of people across Mauritius.

**Google and Mozilla's response:**  https://blog.mozilla.org/netpolicy/files/2021/05/Mozillas-Response-to-the-Mauritian-ICT-Authoritys-Consultation.pdf

**Internet society's comments:** https://www.internetsociety.org/wp-content/uploads/2021/05/Mauritius-IGF-and-Internet-Society-Response-to-Mauritius-ICTA-Consultation-Paper-14-May-2021.pdf

**Joint civil society's comments:** https://www.accessnow.org/cms/assets/uploads/2021/05/Mauritius-ICT-Act-Submission.pdf

**Center for Law and Democracy's comments:** https://www.law-democracy.org/live/wp-content/uploads/2021/05/Mauritius.ICT-Amend.Note_.May21.pdf

**Electronic Frontier Foundation's opinion:** https://www.eff.org/deeplinks/2021/04/proposed-new-internet-law-mauritius-raises-serious-human-rights-concerns

## Conclusion

The decision to tame the social media domestically, will enhance authoritarianism and autocracy. Criticisms of local politicians, allegation of corruption and general grumbles about political issues will trigger censorship and police action.  The digital tracked citizen will no longer be free to protest, oppose or dissent. People will be fined or imprisoned for being too vocal.

The proposal is also a breach to  Mauritians' fundamental right to privacy and online civil liberties are being threatened. We all want an Internet where we are free to meet,create, organise,share, associate,debate and learn. We want to make our voices heard in the way the technology now make possible. No one likes being lied to or misled, or seeing hateful messages directed against them or flooded across our news-feed. We should all have the ability to exercise control over our online environments and to feel empowered by the tools we use.

Fundamentally, this proposal seems to contradict the philosophy of the  Data Protection Act which aims to protect citizens’ private data and  which was adopted in 2017

Platform censorship hurts the less powerful and does not work.



[^1]: https://www.jipitec.eu/issues/jipitec-8-2-2017/4567
[^2]: https://www.eff.org/deeplinks/2020/07/turkeys-new-internet-law-worst-version-germanys-netzdg-yet
[^3]: https://www.techinfrance.fr/comment-gerer-au-mieux-la-relation-client-pour-attirer-des-opportunites-de-croissance-2/
[^4]: www.aides.org/communique/la-prevention-et-la-reduction-des-risques-sur-internet-mises-en-danger-par-la-ppl-avia
[^5]: https://www.nextinpact.com/article/30310/109033-cyberhaine-deux-nouvelles-contributions-adressees-au-conseil-constitutionnel
[^6]: https://www.zdnet.fr/blogs/l-esprit-libre/loi-anti-haine-le-cnnum-la-quadrature-du-net-wikimedia-france-et-9-autres-organisations-s-opposent-ensemble-39897649.htm
[^7]: https://www.ldh-france.org/lettre-ouverte-collective-appelant-a-garantir-nos-libertes-publiques-dans-la-proposition-de-loi-visant-a-lutter-contre-la-haine-sur-internet/
[^8]: https://cnnumerique.fr/CP_regulation_contenus_haineux
[^9]: https://www.nextinpact.com/article/29816/108421-edri-et-fai-europeens-demolissent-proposition-loi-avia-contre-haine-en-ligne
[^10]: https://www.generationlibre.eu/wp-content/uploads/2019/07/CP-Loi-Avia.pdf
[^11]: https://www.nextinpact.com/article/29829/108442-loi-avia-contre-cyberhaine-critiques-commission-europeenne
[^12]: https://freedex.org/wp-content/blogs.dir/2015/files/2019/04/OL-AUS-04.04.19-5.2019-2.pdf
[^13]: https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3443220 

[^14]: https://techcrunch.com/2021/04/28/facebook-hides-posts-calling-for-pm-modis-resignation-in-india/
[^15]: https://www.wsj.com/articles/india-threatens-jail-for-facebook-whatsapp-and-twitter-employees-11614964542?mod=article_inline
[^16]: https://time.com/5935003/india-farmers-protests-twitter/
[^17]: https://www.lexpress.mu/article/328836/vol-donnees-sur-facebook-maurice-citee-parmi-pays-ayant-sollicite-cambridge-analytica

[^18]:  https://www.lexpress.mu/node/392176
[^19]:  http://country.eiu.com/article.aspx?articleid=503373034&Country=Mauritius&topic=Politics&subtopic=Forecast&subsubtopic=Political+stability&u=1&pid=1744509558&oid=1744509558&uid=1
[^20]: https://www.lexpress.mu/article/powers-appointment
[^21]: https://defimedia.info/allegations-de-pot-de-vin-des-cadres-du-ncb-et-de-la-mqa-dans-le-collimateur-de-licac





