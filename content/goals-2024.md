---
title: "Goals 2024"
date: 2023-12-30T17:57:06+04:00
categories: [ '' ]
tags: [ '' ]
draft: false
hidden: true
---

To my accountability buddy,

1. Complete Terraform Associate (003) Certification.
2. Complete AWS Associate Solution Architech (SAA-C002).
3. Make correct use of 2024 Diary.
4. Complete transaction of purchase of Land.
5. Civil Marriage With Samanta.
6. MSCC Talk about control version (Fossil).
7. Migrate Hugo blog to Astro.
8. If Astro Migration complete, then become a speaker for Frontend.mu conference, else for meetup.
9. Be consistent with Blog Articles.
10. Master Nix.
11. Make house plan with help of an Architect.
12. Save money to buy car.
13. Buy wireless mouse.
14. Create playground with microVM.
15. Atleast 4 hikes/trail this year.
16. Camping on the plain of Le Pouce Mountain.
17. Earn Taekwondo first-dant black belt.
18. Progress in Calisthenics and Mobility.
